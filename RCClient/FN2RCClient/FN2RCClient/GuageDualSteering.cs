﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FN2RCClient
{
    public partial class GuageDualSteering : UserControl
    {
        public GuageDualSteering()
        {
            InitializeComponent();

#if EDITUI
            glgControlGuage.DrawingFile = Path.Combine("C:/Users/grahambriggs/source/lbp_frankenNXT20/RCClient/FN2RCClient/FN2RCClient/Resources", "dualsteeringguage.g");
#else
            glgControlGuage.DrawingFile = "./Resources/dualsteeringguage.g";
#endif
            UpdateControl(0.0, 0.0);


        }


        protected double LastFrontValue = -1.0;
        protected double LastRearValue = -1.0;
        protected double UpdateTolerance = 0.5;



        public void UpdateControl(double frontCircle, double rearCircle)
        {
            labelFront.Text = $"Front: {(frontCircle * 360.0).ToString("N2"),6}";
            labelRear.Text = $"Rear:  {(rearCircle * 360.0).ToString("N2"),6}";
            var frontValue = frontCircle * 360.0;
            var rearValue = rearCircle * 360.0 + 180.0;

            if (!LastFrontValue.AlmostEquals(frontValue, UpdateTolerance) || ! LastRearValue.AlmostEquals(rearValue, UpdateTolerance) )
            {
                glgControlGuage.SetDResource("Front/Roll", frontValue);
                glgControlGuage.SetDResource("Rear/Roll", rearValue);
                glgControlGuage.UpdateGlg();
                LastFrontValue = frontValue;
                LastRearValue = rearValue;
            }
            
        }
    }
}
