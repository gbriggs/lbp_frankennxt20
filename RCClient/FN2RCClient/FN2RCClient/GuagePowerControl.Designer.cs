﻿namespace FN2RCClient
{
    partial class GuagePowerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.glgControlGuage = new GenLogic.GlgControl();
            this.labelM2 = new System.Windows.Forms.Label();
            this.labelM1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // glgControlGuage
            // 
            this.glgControlGuage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.glgControlGuage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.glgControlGuage.DrawingFile = "";
            this.glgControlGuage.DrawingObject = null;
            this.glgControlGuage.DrawingURL = "";
            this.glgControlGuage.Enabled = false;
            this.glgControlGuage.HierarchyEnabled = true;
            this.glgControlGuage.Location = new System.Drawing.Point(33, 34);
            this.glgControlGuage.MinimumSize = new System.Drawing.Size(5, 5);
            this.glgControlGuage.Name = "glgControlGuage";
            this.glgControlGuage.SelectEnabled = true;
            this.glgControlGuage.Size = new System.Drawing.Size(189, 189);
            this.glgControlGuage.TabIndex = 3;
            this.glgControlGuage.Text = "glgControl1";
            this.glgControlGuage.Trace2Enabled = false;
            this.glgControlGuage.TraceEnabled = false;
            // 
            // labelM2
            // 
            this.labelM2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelM2.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelM2.ForeColor = System.Drawing.Color.White;
            this.labelM2.Location = new System.Drawing.Point(147, 226);
            this.labelM2.Name = "labelM2";
            this.labelM2.Size = new System.Drawing.Size(103, 28);
            this.labelM2.TabIndex = 5;
            this.labelM2.Text = "label1";
            this.labelM2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelM1
            // 
            this.labelM1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelM1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelM1.ForeColor = System.Drawing.Color.White;
            this.labelM1.Location = new System.Drawing.Point(5, 231);
            this.labelM1.Name = "labelM1";
            this.labelM1.Size = new System.Drawing.Size(103, 23);
            this.labelM1.TabIndex = 4;
            this.labelM1.Text = "label1";
            this.labelM1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 34);
            this.label1.TabIndex = 6;
            this.label1.Text = "Engine Power";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GuagePowerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelM2);
            this.Controls.Add(this.labelM1);
            this.Controls.Add(this.glgControlGuage);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GuagePowerControl";
            this.Size = new System.Drawing.Size(255, 257);
            this.ResumeLayout(false);

        }

        #endregion

        private GenLogic.GlgControl glgControlGuage;
        private System.Windows.Forms.Label labelM2;
        private System.Windows.Forms.Label labelM1;
        private System.Windows.Forms.Label label1;
    }
}
