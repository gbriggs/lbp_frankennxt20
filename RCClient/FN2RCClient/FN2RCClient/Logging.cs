﻿using log4net;
using log4net.Appender;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FN2RCClient
{
    /// <summary>
    /// Define LogLevel to match WiringPiExtension.h log levels
    /// </summary>
    public enum LogLevel
    {
        All = 0,
        VERBOSE,
        TRACE,
        DEBUG,
        INFO,
        WARN,
        ERROR,
        FATAL,
        USER,
        OFF,
    };





    /// <summary>
    /// Logging event args
    /// </summary>
    public class LogEventArgs : EventArgs
    {   
        public LogEventArgs(object sender, string functionName, object data, LogLevel level )
        {
            Time = DateTimeOffset.UtcNow;
            Level = level;
            Thread = System.Threading.Thread.CurrentThread.ManagedThreadId;
            
            Sender = sender;
            Function = functionName;
            Data = data;

            Remote = false;
        }

        public LogEventArgs(long unixTimeMilliseconds, int thread, object sender, LogLevel level, string functionName, object data)
        {
            Time = DateTimeOffset.FromUnixTimeMilliseconds(unixTimeMilliseconds);
            Thread = thread;

            Sender = sender;
            Level = level;
            Function = functionName;
            Data = data;

            Remote = false;
        }

        public LogEventArgs(RemoteLogEventArgs log)
        {
            Time = DateTimeOffset.FromUnixTimeMilliseconds(log.Time);
            Thread = log.Thread;
            Sender = log.Sender;
            Level = log.Level;

            Sender = log.Sender;
            Function = log.Function;
            Data = log.Data;

            Remote = true;
        }

        public DateTimeOffset? Time { get; protected set; }
        public int Thread { get; protected set; }
        public LogLevel Level { get; protected set; }
        
        public object Sender { get; protected set; }
        public string Function { get; protected set; }
        public object Data { get; protected set; }

        public bool Remote { get; protected set; }
    }
    //
    public delegate void LogEventDelegate(object sender, LogEventArgs e);
    public delegate void LogEventsDelegate(object sender, IEnumerable<LogEventArgs> e);


    /// <summary>
    /// Logging Handler Class
    /// Implements a queue so when program object logs, it happens instantly
    /// and queue will be processed to update UI and log to file on the main thread
    /// </summary>
    public class Logging
    {
        //  Log event
        public event LogEventsDelegate LoggedEvents;

        /// <summary>
        /// Start the logging queue
        /// </summary>
        public async Task StartLogging()
        {
            await StopLogging();

            LogsQueue.RemoveAll();

            RunFunctionCancelTokenSource = new CancellationTokenSource();
            RunFunctionTask = RunLogging(RunFunctionCancelTokenSource.Token);

            await RemoteLogging.StartMonitorAsync();
        }


        /// <summary>
        /// Stop the logging queue
        /// </summary>
        public async Task StopLogging()
        {
            await RemoteLogging.StopMonitorAsync();

            if (RunFunctionCancelTokenSource != null)
            {
                RunFunctionCancelTokenSource.Cancel();
                await RunFunctionTask;

                RunFunctionCancelTokenSource = null;
                RunFunctionTask = null;
            }
        }


        /// <summary>
        /// Add a log to the logging queue
        /// </summary>
        public void AddLog(object sender, LogEventArgs log)
        {
            AddLog(log);
        }

        /// <summary>
        /// Add a log to the logging queue
        /// </summary>
        public void AddLog(LogEventArgs log)
        {
            LogsQueue.Enqueue(log);
            NotifyAddedLog.Release();
        }


        /// <summary>
        /// Add a log to the logging queue
        /// </summary>
        public void AddLog(RemoteLogEventArgs log)
        {
            AddLog(new LogEventArgs(log));
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public Logging()
        {
            NotifyAddedLog = new SemaphoreSlim(0);
            LogsQueue = new ConcurrentQueue<LogEventArgs>();

            RemoteLogging = new RemoteLogMonitor();

            RemoteLogging.RemoteLogReceived += OnRemoteLogReceived; ;
            RemoteLogging.Log += OnLog;
        }

        
        //  Run function task
        protected CancellationTokenSource RunFunctionCancelTokenSource { get; set; }
        protected Task RunFunctionTask { get; set; }

        //  Queue
        protected SemaphoreSlim NotifyAddedLog { get; set; }
        protected ConcurrentQueue<LogEventArgs> LogsQueue { get; set; }


        //  Remote Log Monitor
        RemoteLogMonitor RemoteLogging { get; set; }


        /// <summary>
        /// Handler for component logging
        /// </summary>
        private void OnLog(object sender, LogEventArgs e)
        {
            AddLog(e);
        }


        /// <summary>
        /// Handler for remote log monitor logging
        /// </summary>
        private void OnRemoteLogReceived(object sender, RemoteLogEventArgs e)
        {
            AddLog(e);
        }


        /// <summary>
        /// Logging queue processing run function
        /// </summary>
        private async Task RunLogging(CancellationToken cancelToken)
        {
            try
            {
                while (!cancelToken.IsCancellationRequested)
                {
                    await NotifyAddedLog.WaitAsync(cancelToken);

                    List<LogEventArgs> allEvents = new List<LogEventArgs>();
                    while (!LogsQueue.IsEmpty)
                    {
                        if (LogsQueue.TryDequeue(out var nextLog))
                        {
                            allEvents.AddRange(GenerateLogsForLogEvent(nextLog));   
                        }
                    }

                    LoggedEvents?.Invoke(this, allEvents);

                    await (LogToLog4(allEvents));
                }
            }
            catch (OperationCanceledException)
            { }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine($"Exception in logging: {e}.");
            }

        }


        /// <summary>
        /// Generate a collection of logs for this one log
        /// will return a collection of logs with this log as first item
        /// and if the data is an excetion, will generate additional logs for the inner exceptions
        /// </summary>
        private IEnumerable<LogEventArgs> GenerateLogsForLogEvent(LogEventArgs log)
        {
            //  put this log in a list
            List<LogEventArgs> logsList = new List<LogEventArgs>() { log };

            //  also add inner exceptions if this log happens to be an exception
            if (log.Data != null)
            {
                var logException = log.Data as Exception;
                if (logException != null)
                {
                    var inex = logException.InnerException;
                    while (inex != null)
                    {
                        LogEventArgs insertArg = new LogEventArgs(log.Sender, log.Function, inex, log.Level);
                        logsList.Add(insertArg);
                        inex = inex.InnerException;
                    }
                }
            }

            return logsList;
        }


        /// <summary>
        /// Logging using Log4 framework to generate log files
        /// </summary>
        private static readonly log4net.ILog logSystem = log4net.LogManager.GetLogger("SystemLogger");

        /// <summary>
        /// Log to the Log4 Framework
        /// </summary>
        private async Task LogToLog4(IEnumerable<LogEventArgs> logs)
        {
            await Task.Run(() =>
            {
                foreach (var log in logs)
                {  
                    switch (log.Level)
                    {
                        case LogLevel.VERBOSE:
                        case LogLevel.TRACE:
                        case LogLevel.DEBUG:
                            logSystem.Debug(log.FormatLogForFile());
                            break;

                        case LogLevel.INFO:
                        case LogLevel.USER:
                            logSystem.Info(log.FormatLogForFile());
                            break;

                        case LogLevel.WARN:
                            logSystem.Warn(log.FormatLogForFile());
                            break;

                        case LogLevel.ERROR:
                            logSystem.Error(log.FormatLogForFile());
                            break;

                        default:
                        case LogLevel.FATAL:
                            logSystem.Fatal(log.FormatLogForFile());
                            break;
                    }
                }
            }
                );

        }
    }

    /// <summary>
    /// Logging object extension methods
    /// </summary>
    public static class LoggingExtensionMethods
    {
        public static string FormatLogForFile(this LogEventArgs value)
        {
            return $"{value.Time.FormatTimeHoursHHmmssfff()},[{ value.Thread.ToString().Right(3) }],{value.Level},{value.Sender ?? "senderUnknown"},{value.Function ?? "functionUnknown"},{value.Data ?? "dataUnknown"}";
        }

        public static string FormatLogForConsole(this LogEventArgs value)
        {
            return $"{value.Time.FormatTimeHoursHHmmssfff()} [{string.Format("{0,3}", value.Thread.ToString().Right(3))}] {string.Format("{0,7}", value.Level)}   {string.Format("{0,-20}", value.Sender.ToString().Left(19) ?? "senderUnknown")} {string.Format("{0,-20}", value.Function.Left(19) ?? "functionUnknown")} {value.Data ?? "dataUnknown"}";
        }


        /// <summary>
        /// Log Colour
        /// </summary>
        public static Color LogColour(this LogLevel level)
        {
            switch (level)
            {
                case LogLevel.VERBOSE:
                    return Color.DarkCyan;

                case LogLevel.TRACE:
                    return Color.Cyan;

                case LogLevel.DEBUG:
                    return Color.LightGreen;

                case LogLevel.INFO:
                    return Color.White;

                case LogLevel.USER:
                    return Color.LightBlue;

                case LogLevel.WARN:
                    return Color.Red;

                case LogLevel.ERROR:
                    return Color.White;

                case LogLevel.FATAL:
                    return Color.Yellow;

                default:
                    return Color.White;

            }
        }

        /// <summary>
        /// Log Colour
        /// </summary>
        public static Color BackgrondColour(this LogLevel level, bool remote)
        {
            if (remote)
            {
                switch (level)
                {
                    case LogLevel.VERBOSE:
                        return Color.FromArgb(10, 10, 16);

                    case LogLevel.TRACE:
                        return Color.FromArgb(10, 10, 16);

                    case LogLevel.DEBUG:
                        return Color.FromArgb(10, 10, 16);

                    case LogLevel.INFO:
                        return Color.FromArgb(10, 10, 16);

                    case LogLevel.USER:
                        return Color.FromArgb(10, 10, 16);

                    case LogLevel.WARN:
                        return Color.FromArgb(10, 10, 16);

                    case LogLevel.ERROR:
                        return Color.IndianRed;

                    case LogLevel.FATAL:
                        return Color.IndianRed;

                    default:
                        return Color.White;

                }
            }
            else
            {
                switch (level)
                {
                    case LogLevel.VERBOSE:
                        return Color.Black;

                    case LogLevel.TRACE:
                        return Color.Black;

                    case LogLevel.DEBUG:
                        return Color.Black;

                    case LogLevel.INFO:
                        return Color.Black;

                    case LogLevel.USER:
                        return Color.Black;

                    case LogLevel.WARN:
                        return Color.Black;

                    case LogLevel.ERROR:
                        return Color.Red;

                    case LogLevel.FATAL:
                        return Color.Red;

                    default:
                        return Color.White;

                }
            }
        }
    }


}
