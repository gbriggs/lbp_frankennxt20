﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FN2RCClient
{

    public class RobotConnectionStatus
    {
        public RobotConnectionStatus()
        {

        }

        public string Eth0Connection()
        {
            if ( Eth0.Length > 0 )
            {
                return Eth0;
            }
            else
            {
                return " - not connected - ";
            }
        }

        public string WLan0Connection()
        {
            if (Wlan0.Length > 0 && WlanMode.Length > 0)
            {
                return $"{Wlan0} {WlanMode}";
            }
            else
            {
                return " - NC - ";
            }
        }

        public string Eth0 { get; set; }
        public string Wlan0 { get; set; }
        public string WlanMode { get; set; }
        public TimeSpan PingSpeed { get; set; }
    }


    /// <summary>
    /// Robot motors status
    /// </summary>
    public class RobotEngineRoomStatus
    {
        public RobotEngineRoomStatus()
        {
            
        }


        //  Identity
        public string Name { get; set; }

        //  Engine Room
        public double EnginePower { get; set; }

        public double EngineMotor1Power { get; set; }

        public double EngineMotor2Power { get; set; }

        public double EngineMotor1RPM { get; set; }

        public double EngineMotor2RPM { get; set; }

        public int Gear { get; set; }

        public int Direction { get; set; }

        //  Steering
        public double FrontSteering { get; set; }

        public double RearSteering { get; set; }

        //  Overrides
        public override bool Equals(object obj)
        {
            if (obj is RobotEngineRoomStatus status)
                return this.CompareRobotEngineRoomStatus(status);

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }


    /// <summary>
    /// Robot Button status
    /// </summary>
    public class RobotButtonStatus
    {
        public RobotButtonStatus()
        {

        }

        //  Identity
        public string Name { get; set; }

        //  Brick Buttons
        public bool LeftBtn { get; set; }

        public bool EnterBtn { get; set; }

        public bool RightBtn { get; set; }

        public bool DownBtn { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is RobotButtonStatus status)
                return this.CompareButtonStatus(status);

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }



    /// <summary>
    /// Robot controller status
    /// </summary>
    public class RobotControllerStatus
    {
        public RobotControllerStatus()
        {
           
        }


        //  Identity
        public string Name { get; set; }

        //  Hardware Status
        public double MainBattVoltage { get; set; }

        public double CpuTemp { get; set; }

        public double CpuUsage { get; set; }



        public override bool Equals(object obj)
        {
            if (obj is RobotControllerStatus status)
                return this.CompareControllerStatus(status);

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }


    


    /// <summary>
    /// Status extension methods
    /// </summary>
    public static class RobotStatusExtensionMethods
    {
        public static bool CompareRobotEngineRoomStatus(this RobotEngineRoomStatus value, RobotEngineRoomStatus compare)
        {
            //  engine power, tolerance 1%
            if (value.Name?.CompareTo(compare.Name) == 0 &&
                value.EnginePower.AlmostEquals(compare.EnginePower, 0.01) &&
                value.EngineMotor1Power.AlmostEquals(compare.EngineMotor1Power, 0.01) &&
                value.EngineMotor2Power.AlmostEquals(compare.EngineMotor2Power, 0.01) &&
                value.EngineMotor1RPM.AlmostEquals(compare.EngineMotor1RPM, 0.1) &&
                value.EngineMotor2RPM.AlmostEquals(compare.EngineMotor2RPM, 0.1) &&
                value.Gear == compare.Gear &&
                value.Direction == compare.Direction &&
                value.FrontSteering.AlmostEquals(compare.FrontSteering, 0.0001) &&
                value.RearSteering.AlmostEquals(compare.RearSteering, 0.0001))
            {
                return true;
            }

            return false;
        }

        public static bool CompareButtonStatus(this RobotButtonStatus value, RobotButtonStatus compare)
        {
            //  engine power, tolerance 1%
            if (value.Name?.CompareTo(compare.Name) == 0 &&
                value.LeftBtn == compare.LeftBtn &&
                value.EnterBtn == compare.EnterBtn && 
                value.RightBtn == compare.RightBtn && 
                value.DownBtn == compare.DownBtn)
            {
                return true;
            }

            return false;
        }
        
        public static bool CompareControllerStatus(this RobotControllerStatus value, RobotControllerStatus compare)
        {
            //  engine power, tolerance 1%
            if (value.Name?.CompareTo(compare.Name) == 0 &&
                value.MainBattVoltage.AlmostEquals(compare.MainBattVoltage, 0.01) &&
                value.CpuTemp.AlmostEquals(compare.CpuTemp, 0.1) &&
                value.CpuUsage.AlmostEquals(compare.CpuUsage, 0.1))
            {
                return true;
            }

            return false;
        }
    }
}
