﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace FN2RCClient
{
    /// <summary>
    /// Engine room status event
    /// </summary>
    public class RobotEngineRoomStatusEventArgs : EventArgs
    {
        public RobotEngineRoomStatusEventArgs(RobotEngineRoomStatus status)
        {
            Status = status;
        }

        public RobotEngineRoomStatus Status { get; set; }
    }
    //
    public delegate void RobotEngineRoomStatusUpdateDelegate(object sender, RobotEngineRoomStatusEventArgs e);


    /// <summary>
    /// Controller status event
    /// </summary>
    public class RobotControllerStatusEventArgs : EventArgs
    {
        public RobotControllerStatusEventArgs(RobotControllerStatus status)
        {
            Status = status;
        }

        public RobotControllerStatus Status { get; set; }
    }
    //
    public delegate void RobotControllerStatusUpdateDelegate(object sender, RobotControllerStatusEventArgs e);


    /// <summary>
    /// Connection status event
    /// </summary>
    public class RobotConnectionStatusEventArgs : EventArgs
    {
        public RobotConnectionStatusEventArgs(RobotConnectionStatus status)
        {
            Status = status;
        }

        public RobotConnectionStatus Status { get; set; }
    }
    //
    public delegate void RobotConnectionStatusUpdateDelegate(object sender, RobotConnectionStatusEventArgs e);





    class RobotStatusMonitor
    {
        public event LogEventDelegate Log;

     
        public event RobotEngineRoomStatusUpdateDelegate RobotStatusEnginePowerChanged;
        public event RobotEngineRoomStatusUpdateDelegate RobotStatusMotor1RpmChanged;
        public event RobotEngineRoomStatusUpdateDelegate RobotStatusMotor2RpmChanged;
        public event RobotEngineRoomStatusUpdateDelegate RobotStatusGearChanged;
        public event RobotEngineRoomStatusUpdateDelegate RobotStatusFrontSteeringChanged;
        public event RobotEngineRoomStatusUpdateDelegate RobotStatusRearSteeringChanged;

         public event RobotControllerStatusUpdateDelegate RobotControllerStatusChanged;

        public event RobotConnectionStatusUpdateDelegate RobotConnectionStatusChanged;

        /// <summary>
        /// Start the monitor
        /// </summary>
        public async Task StartMonitorAsync()
        {          
            await StopMonitorAsync();

            MonitorCancelTokenSource = new CancellationTokenSource();
            MonitorRunTask = RunStatusMonitor(MonitorCancelTokenSource.Token);


        }


        /// <summary>
        /// Stop the monitor
        /// </summary>
        public async Task StopMonitorAsync()
        {
            if (MonitorCancelTokenSource != null)
            {
                MonitorCancelTokenSource.Cancel();

                if (MonitorRunTask != null)
                    await MonitorRunTask;

                MonitorCancelTokenSource = null;
                MonitorRunTask = null;
            }
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public RobotStatusMonitor()
        {
            LatestEngineRoomStatus = new RobotEngineRoomStatus();
            LatestControllerStatus = new RobotControllerStatus();
        }

        CancellationTokenSource MonitorCancelTokenSource { get; set; }
        Task MonitorRunTask { get; set; }

        public RobotEngineRoomStatus LatestEngineRoomStatus { get; protected set; }
        public RobotControllerStatus LatestControllerStatus { get; protected set; }

        protected async Task RunStatusMonitor(CancellationToken cancelToken)
        {
            try
            {
                using (var udpClient = new UdpClient(48886))
                {

                    try
                    {
                        udpClient.JoinMulticastGroup(IPAddress.Parse("234.5.6.7"));
                        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                        sw.Start();

                        while (!cancelToken.IsCancellationRequested)
                        {
                            var receivedResult = await udpClient.ReceiveAsync();
                            var receivedString = Encoding.ASCII.GetString(receivedResult.Buffer).Split('?');

                            if (receivedString.Count() == 2)
                            {
                                var responseArgs = HttpUtility.ParseQueryString(receivedString[1]);

                                switch (receivedString[0])
                                {
                                    case "motorstatus":
                                        {
                                            try
                                            {
                                                var statusString = responseArgs.Get("status");
                                                var status = JsonConvert.DeserializeObject<RobotEngineRoomStatus>(statusString);
                                                var timeTag = responseArgs.Get("time");
                                                SendEngineRoomStatusChangedEvents(status);
                                                //Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", $"Received motor status {DateTimeOffset.UtcNow.ToLocalTime().ToString("HH:mm:ss.fff")} send time {DateTimeOffset.FromUnixTimeMilliseconds(long.Parse(timeTag)).ToLocalTime().ToString("HH:mm:ss.fff")}.", LogLevel.VERBOSE));
                                            }
                                            catch (Exception ex)
                                            {
                                                Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", ex, LogLevel.ERROR));
                                            }
                                        }
                                        break;

                                    case "networkstatus":
                                        {
                                            try
                                            {
                                                var eth0String = responseArgs.Get("eth0");
                                                var wlan0String = responseArgs.Get("wlan0");
                                                var wlanModeString = responseArgs.Get("wlanmode");
                                                var timeTag = responseArgs.Get("time");

                                                RobotConnection.IpAddressEth0 = eth0String;
                                                RobotConnection.IpAddressWlan0 = wlan0String;

                                                SendConnectionStatusChangedEvents(eth0String, wlan0String, wlanModeString, timeTag);
                                            }
                                            catch (Exception ex)
                                            {
                                                Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", ex, LogLevel.ERROR));
                                            }
                                            
                                        }
                                        break;

                                    case "buttonstatus":
                                        {
                                            try
                                            {
                                                var statusString = responseArgs.Get("status");
                                                var status = JsonConvert.DeserializeObject<RobotButtonStatus>(statusString);
                                                var timeTag = responseArgs.Get("time");

                                                Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", $"Received button status {DateTimeOffset.UtcNow.ToLocalTime().ToString("HH:mm:ss.fff")} send time {DateTimeOffset.FromUnixTimeMilliseconds(long.Parse(timeTag)).ToLocalTime().ToString("HH:mm:ss.fff")}.", LogLevel.VERBOSE));
                                            }
                                            catch (Exception ex)
                                            {
                                                Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", ex, LogLevel.ERROR));
                                            }
                                        }
                                        break;

                                    case "controlstatus":
                                        {
                                            try
                                            {
                                                var statusString = responseArgs.Get("status");
                                                var status = JsonConvert.DeserializeObject<RobotControllerStatus>(statusString);
                                                var timeTag = responseArgs.Get("time");
                                                SendControllerStatusChangedEvents(status);
                                                //Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", $"Received controller status {DateTimeOffset.UtcNow.ToLocalTime().ToString("HH:mm:ss.fff")} send time {DateTimeOffset.FromUnixTimeMilliseconds(long.Parse(timeTag)).ToLocalTime().ToString("HH:mm:ss.fff")}.", LogLevel.VERBOSE));
                                            }
                                            catch (Exception ex)
                                            {
                                                Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", ex, LogLevel.ERROR));
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", ex, LogLevel.ERROR));
                    }
                }
            }
            catch (Exception e)
            {
                Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", e, LogLevel.ERROR));
            }
        }

        private async void SendConnectionStatusChangedEvents(string eth0String, string wlan0String, string wlanModeString, string timeTag)
        {
            try
            {
                if (!RobotConnection.IpAddressIsValid )
                {
                    //Log?.Invoke(this, new LogEventArgs(this, "SendConnectionStatusChangedEvents", "Robot IP address is not known, skipping ping.", LogLevel.TRACE));
                    return;
                }
                
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                var response = await TcpRequest.GetTcpResponse(RobotConnection.IpAddress, RobotConnection.ComPort, "ping\n", 5000, 5000);
                sw.Stop();
               // Log?.Invoke(this, new LogEventArgs(this, "RunStatusMonitor", $"Received IP status: eth0 {eth0String} wlan0 {wlan0String} {wlanModeString}. Send time {DateTimeOffset.FromUnixTimeMilliseconds(long.Parse(timeTag)).ToLocalTime().ToString("HH:mm:ss.fff")}", LogLevel.VERBOSE));
                RobotConnectionStatus status = new RobotConnectionStatus()
                {
                    Eth0 = eth0String,
                    Wlan0 = wlan0String,
                    WlanMode = wlanModeString,
                    PingSpeed = sw.Elapsed,
                };

                RobotConnectionStatusChanged?.Invoke(this, new RobotConnectionStatusEventArgs(status));
            }
            catch (Exception e)
            {
                Log?.Invoke(this, new LogEventArgs(this, "SendConnectionStatusChangedEvents", e, LogLevel.ERROR));
            }
        }

        private void SendControllerStatusChangedEvents(RobotControllerStatus status)
        {
            if ( ! LatestControllerStatus.Equals(status) )
            {
                RobotControllerStatusChanged?.Invoke(this, new RobotControllerStatusEventArgs(status));
            }

            LatestControllerStatus = status;
        }


        //protected async Task RunConnectionPing(CancellationToken cancelToken)
        //{

        //}


        protected void SendEngineRoomStatusChangedEvents(RobotEngineRoomStatus newStatus)
        {
            //  engine power, tolerance 1%
            if ( !LatestEngineRoomStatus.EnginePower.AlmostEquals(newStatus.EnginePower, 0.01) || 
                ! LatestEngineRoomStatus.EngineMotor1Power.AlmostEquals(newStatus.EngineMotor1Power, 0.01 ) ||
                ! LatestEngineRoomStatus.EngineMotor2Power.AlmostEquals(newStatus.EngineMotor2Power, 0.01))
            {
                RobotStatusEnginePowerChanged?.Invoke(this, new RobotEngineRoomStatusEventArgs(newStatus));
                LatestEngineRoomStatus.EnginePower = newStatus.EnginePower;
                LatestEngineRoomStatus.EngineMotor1Power = newStatus.EngineMotor1Power;
                LatestEngineRoomStatus.EngineMotor2Power = newStatus.EngineMotor2Power;
            }

            //  motor RPM tolerance 0.1
            if ( ! LatestEngineRoomStatus.EngineMotor1RPM.AlmostEquals(newStatus.EngineMotor1RPM, 0.1))
            {
                RobotStatusMotor1RpmChanged?.Invoke(this, new RobotEngineRoomStatusEventArgs(newStatus));
                LatestEngineRoomStatus.EngineMotor1RPM = newStatus.EngineMotor1RPM;
            }

            if ( ! LatestEngineRoomStatus.EngineMotor2RPM.AlmostEquals(newStatus.EngineMotor2RPM, 0.1))
            {
                RobotStatusMotor2RpmChanged?.Invoke(this, new RobotEngineRoomStatusEventArgs(newStatus));
                LatestEngineRoomStatus.EngineMotor2RPM = newStatus.EngineMotor2RPM;
            }

            //  gear
            if ( LatestEngineRoomStatus.Gear != newStatus.Gear || LatestEngineRoomStatus.Direction != newStatus.Direction )
            {
                RobotStatusGearChanged?.Invoke(this, new RobotEngineRoomStatusEventArgs(newStatus));
                LatestEngineRoomStatus.Gear = newStatus.Gear;
            }

            //  steering tolerance 0.0001
            if ( ! LatestEngineRoomStatus.FrontSteering.AlmostEquals(newStatus.FrontSteering, 0.0001) )
            {
                RobotStatusFrontSteeringChanged?.Invoke(this, new RobotEngineRoomStatusEventArgs(newStatus));
                LatestEngineRoomStatus.FrontSteering = newStatus.FrontSteering;
            }

            if (!LatestEngineRoomStatus.RearSteering.AlmostEquals(newStatus.RearSteering, 0.0001))
            {
                RobotStatusRearSteeringChanged?.Invoke(this, new RobotEngineRoomStatusEventArgs(newStatus));
                LatestEngineRoomStatus.RearSteering = newStatus.RearSteering;
            }
        }

    }
}
