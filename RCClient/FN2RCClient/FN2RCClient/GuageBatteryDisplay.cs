﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FN2RCClient
{
    public partial class GuageBatteryDisplay : UserControl
    {
        public GuageBatteryDisplay()
        {
            InitializeComponent();

#if EDITUI
            glgControlGuage.DrawingFile = Path.Combine("C:/Users/grahambriggs/source/lbp_frankenNXT20/RCClient/FN2RCClient/FN2RCClient/Resources", "batdisplayguage.g");
#else
            glgControlGuage.DrawingFile = "./Resources/batdisplayguage.g";
#endif
            UpdateControl(0.0);

        }

        protected double LastValue = -1.0;
        protected double UpdateTolerance = .1;

        public void UpdateControl(double voltage)
        {
            if (!LastValue.AlmostEquals(voltage, UpdateTolerance))
            {

                glgControlGuage.SetDResource("Display/Value", voltage);
                glgControlGuage.UpdateGlg();
                LastValue = voltage;
            }
        }

        //      TODO - set LED color for voltage
        //   glgControlGuage.SetGResource("Display/LedOffColor", 1.0, 1.0, 1.0 );
    }
}