﻿namespace FN2RCClient
{
    partial class GuageRpmControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLabel = new System.Windows.Forms.Label();
            this.glgControlGuage = new GenLogic.GlgControl();
            this.labelValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelLabel
            // 
            this.labelLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLabel.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLabel.ForeColor = System.Drawing.Color.White;
            this.labelLabel.Location = new System.Drawing.Point(4, 222);
            this.labelLabel.Name = "labelLabel";
            this.labelLabel.Size = new System.Drawing.Size(219, 33);
            this.labelLabel.TabIndex = 0;
            this.labelLabel.Text = "label1";
            this.labelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // glgControlGuage
            // 
            this.glgControlGuage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.glgControlGuage.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.glgControlGuage.DrawingFile = "";
            this.glgControlGuage.DrawingObject = null;
            this.glgControlGuage.DrawingURL = "";
            this.glgControlGuage.Enabled = false;
            this.glgControlGuage.HierarchyEnabled = true;
            this.glgControlGuage.Location = new System.Drawing.Point(4, 4);
            this.glgControlGuage.MinimumSize = new System.Drawing.Size(5, 5);
            this.glgControlGuage.Name = "glgControlGuage";
            this.glgControlGuage.SelectEnabled = true;
            this.glgControlGuage.Size = new System.Drawing.Size(219, 219);
            this.glgControlGuage.TabIndex = 2;
            this.glgControlGuage.Text = "glgControl1";
            this.glgControlGuage.Trace2Enabled = false;
            this.glgControlGuage.TraceEnabled = false;
            // 
            // labelValue
            // 
            this.labelValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelValue.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValue.ForeColor = System.Drawing.Color.White;
            this.labelValue.Location = new System.Drawing.Point(4, 258);
            this.labelValue.Name = "labelValue";
            this.labelValue.Size = new System.Drawing.Size(219, 21);
            this.labelValue.TabIndex = 3;
            this.labelValue.Text = "label1";
            this.labelValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GuageRpmControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.labelValue);
            this.Controls.Add(this.glgControlGuage);
            this.Controls.Add(this.labelLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GuageRpmControl";
            this.Size = new System.Drawing.Size(227, 285);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelLabel;
        private GenLogic.GlgControl glgControlGuage;
        private System.Windows.Forms.Label labelValue;
    }
}
