﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FN2RCClient
{
    public static class StringExtensionMethods
    {
        public static bool CheckFnxtResponse(this string value)
        {
            if ( value != null && value.Length > 2 )
            {
                var response = value.Substring(0, 3);
                if (response == "ACK")
                    return true;
            }
            return false;
        }

        public static string GetFnResponse(this string value)
        {
            if (value != null && value.Length > 3)
            {
                return value.Substring(3);
            }
            return "";
        }

        public static string Left(this string value, int count)
        {
            return value.Substring(0, Math.Min(count, value.Length));
        }

        public static string Right(this string value, int count)
        {
            if (value.Length <= count)
                return value;
            return value.Substring(value.Length - count, count);
        }
    }
}
