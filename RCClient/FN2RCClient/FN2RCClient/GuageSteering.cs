﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FN2RCClient
{
    public partial class GuageSteering : UserControl
    {
        public GuageSteering()
        {
            InitializeComponent();

#if EDITUI
            glgControlGuage.DrawingFile = Path.Combine("C:/Users/grahambriggs/source/lbp_frankenNXT20/RCClient/FN2RCClient/FN2RCClient/Resources", "turnguage.g");
#else
            glgControlGuage.DrawingFile = "./Resources/turnguage.g";
#endif
            UpdateControl(0.0);
        }


        public void SetLabel(string label, bool reverse)
        {
            labelLabel.Text = label;
            Reversed = reverse;
        }

        protected double LastValue = -1.0;
        protected double UpdateTolerance = (1.0/360.0);
        protected bool Reversed;

        public void UpdateControl(double circle)
        {
            double degrees = circle * 360.0;
            labelValue.Text = $"{degrees.ToString("N2")}";

            if (Reversed)
                degrees += 180.0;

            if (!LastValue.AlmostEquals(circle, UpdateTolerance))
            {
                glgControlGuage.SetDResource("Roll", degrees);
                glgControlGuage.UpdateGlg();
                LastValue = circle;
            }
            
        }
    }
}
