﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FN2RCClient
{
    public class RobotConnection
    {
        public static readonly int ComPort = 47889;

        public static string IpAddressEth0 = "";
        public static string IpAddressWlan0 = "";

        public static bool IpAddressIsValid
        {
            get
            {
                return IpAddressEth0.Length > 0 || IpAddressWlan0.Length > 0;
            }
        }

        public static string IpAddress
        {
            get
            {
                if (IpAddressEth0.Length > 0)
                    return IpAddressEth0;
                else
                    return IpAddressWlan0;
            }
        }

    }
}
