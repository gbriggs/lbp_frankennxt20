﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FN2RCClient
{
    public partial class GuageCpuUsage : UserControl
    {
        public GuageCpuUsage()
        {
            InitializeComponent();

#if EDITUI
            glgControlGuage.DrawingFile = Path.Combine("C:/Users/grahambriggs/source/lbp_frankenNXT20/RCClient/FN2RCClient/FN2RCClient/Resources", "cpuguage.g");
#else
            glgControlGuage.DrawingFile = "./Resources/cpuguage.g";
#endif
            UpdateControl(0.0);

        }

        protected double LastValue = -1.0;
        protected double UpdateTolerance = 1.0;

        public void UpdateControl(double cpuPercent)
        {
            if (!LastValue.AlmostEquals(cpuPercent, UpdateTolerance))
            {

                glgControlGuage.SetDResource("Value", cpuPercent);
                glgControlGuage.UpdateGlg();
                LastValue = cpuPercent;
            }
            labelValue.Text = $"{cpuPercent.ToString("N1"),3}%";
        }
    }
}
