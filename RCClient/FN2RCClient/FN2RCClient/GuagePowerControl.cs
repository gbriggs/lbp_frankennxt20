﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FN2RCClient
{
    public partial class GuagePowerControl : UserControl
    {
        public GuagePowerControl()
        {
            InitializeComponent();
#if EDITUI
            glgControlGuage.DrawingFile = Path.Combine("C:/Users/grahambriggs/source/lbp_frankenNXT20/RCClient/FN2RCClient/FN2RCClient/Resources", "powerguage.g");
#else
            glgControlGuage.DrawingFile = "./Resources/powerguage.g";
#endif

        }

        protected double LastEnginePowerValue = -1.0;
        protected double LastM1PowerValue = -1.0;
        protected double LastM2PowerValue = -1.0;
        protected double UpdateTolerance = 0.01;

        public void UpdateControl(RobotEngineRoomStatus status )
        {
            if (!LastEnginePowerValue.AlmostEquals(status.EnginePower, UpdateTolerance))
            {

                glgControlGuage.SetDResource("Value", status.EnginePower*100);
                glgControlGuage.UpdateGlg();
                LastEnginePowerValue = status.EnginePower;
            }
            if ( ! LastM1PowerValue.AlmostEquals(status.EngineMotor1Power, UpdateTolerance) )
            {
                labelM1.Text = $"M 1: {status.EngineMotor1Power.ToString("N2"),5}";
                LastM1PowerValue = status.EngineMotor1Power;
            }
            if (!LastM2PowerValue.AlmostEquals(status.EngineMotor2Power, UpdateTolerance))
            {
                labelM2.Text = $"M 2: {status.EngineMotor2Power.ToString("N2"),5}";
                LastM2PowerValue = status.EngineMotor2Power;
            }
        }
    }
}
