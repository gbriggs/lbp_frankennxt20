﻿namespace FN2RCClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelConnectionStatus = new System.Windows.Forms.Label();
            this.listViewLogs = new System.Windows.Forms.ListView();
            this.comboBoxLogLevel = new System.Windows.Forms.ComboBox();
            this.groupBoxLogging = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxLogLevelWpi = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxLogLevelRemote = new System.Windows.Forms.ComboBox();
            this.panelConnectionStatus = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panelEngine = new System.Windows.Forms.Panel();
            this.labelTransmission = new System.Windows.Forms.Label();
            this.rpmGuage1 = new FN2RCClient.GuageRpmControl();
            this.guagePowerControl1 = new FN2RCClient.GuagePowerControl();
            this.rpmGuage2 = new FN2RCClient.GuageRpmControl();
            this.panelSteering = new System.Windows.Forms.Panel();
            this.guageDualSteering1 = new FN2RCClient.GuageDualSteering();
            this.panelControllerStatus = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.guageBatteryDisplay = new FN2RCClient.GuageBatteryDisplay();
            this.guageCpuTemp = new FN2RCClient.GuageCpuTemp();
            this.guageCpuUsage = new FN2RCClient.GuageCpuUsage();
            this.groupBoxLogging.SuspendLayout();
            this.panelConnectionStatus.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelEngine.SuspendLayout();
            this.panelSteering.SuspendLayout();
            this.panelControllerStatus.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelConnectionStatus
            // 
            this.labelConnectionStatus.BackColor = System.Drawing.Color.Gray;
            this.labelConnectionStatus.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnectionStatus.ForeColor = System.Drawing.Color.White;
            this.labelConnectionStatus.Location = new System.Drawing.Point(18, 34);
            this.labelConnectionStatus.Name = "labelConnectionStatus";
            this.labelConnectionStatus.Size = new System.Drawing.Size(269, 105);
            this.labelConnectionStatus.TabIndex = 0;
            this.labelConnectionStatus.Text = "Waiting for connection ...";
            // 
            // listViewLogs
            // 
            this.listViewLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewLogs.BackColor = System.Drawing.Color.Black;
            this.listViewLogs.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewLogs.HideSelection = false;
            this.listViewLogs.Location = new System.Drawing.Point(0, 0);
            this.listViewLogs.MultiSelect = false;
            this.listViewLogs.Name = "listViewLogs";
            this.listViewLogs.Size = new System.Drawing.Size(1123, 190);
            this.listViewLogs.TabIndex = 4;
            this.listViewLogs.UseCompatibleStateImageBehavior = false;
            // 
            // comboBoxLogLevel
            // 
            this.comboBoxLogLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxLogLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLogLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxLogLevel.FormattingEnabled = true;
            this.comboBoxLogLevel.Location = new System.Drawing.Point(454, 196);
            this.comboBoxLogLevel.Name = "comboBoxLogLevel";
            this.comboBoxLogLevel.Size = new System.Drawing.Size(116, 24);
            this.comboBoxLogLevel.TabIndex = 5;
            this.comboBoxLogLevel.SelectedIndexChanged += new System.EventHandler(this.comboBoxLogLevel_SelectedIndexChanged);
            // 
            // groupBoxLogging
            // 
            this.groupBoxLogging.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxLogging.BackColor = System.Drawing.Color.DimGray;
            this.groupBoxLogging.Controls.Add(this.label7);
            this.groupBoxLogging.Controls.Add(this.comboBoxLogLevelWpi);
            this.groupBoxLogging.Controls.Add(this.label3);
            this.groupBoxLogging.Controls.Add(this.label2);
            this.groupBoxLogging.Controls.Add(this.comboBoxLogLevelRemote);
            this.groupBoxLogging.Controls.Add(this.listViewLogs);
            this.groupBoxLogging.Controls.Add(this.comboBoxLogLevel);
            this.groupBoxLogging.Location = new System.Drawing.Point(33, 699);
            this.groupBoxLogging.Name = "groupBoxLogging";
            this.groupBoxLogging.Size = new System.Drawing.Size(1122, 225);
            this.groupBoxLogging.TabIndex = 6;
            this.groupBoxLogging.TabStop = false;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(852, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 16);
            this.label7.TabIndex = 10;
            this.label7.Text = "Remote wpi Log Level";
            // 
            // comboBoxLogLevelWpi
            // 
            this.comboBoxLogLevelWpi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxLogLevelWpi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLogLevelWpi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxLogLevelWpi.FormattingEnabled = true;
            this.comboBoxLogLevelWpi.Location = new System.Drawing.Point(1001, 196);
            this.comboBoxLogLevelWpi.Name = "comboBoxLogLevelWpi";
            this.comboBoxLogLevelWpi.Size = new System.Drawing.Size(116, 24);
            this.comboBoxLogLevelWpi.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(593, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Remote Log Level";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(381, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Log Level";
            // 
            // comboBoxLogLevelRemote
            // 
            this.comboBoxLogLevelRemote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxLogLevelRemote.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLogLevelRemote.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxLogLevelRemote.FormattingEnabled = true;
            this.comboBoxLogLevelRemote.Location = new System.Drawing.Point(717, 196);
            this.comboBoxLogLevelRemote.Name = "comboBoxLogLevelRemote";
            this.comboBoxLogLevelRemote.Size = new System.Drawing.Size(116, 24);
            this.comboBoxLogLevelRemote.TabIndex = 7;
            // 
            // panelConnectionStatus
            // 
            this.panelConnectionStatus.BackgroundImage = global::FN2RCClient.Properties.Resources.legoPlateLightGrey1200x700;
            this.panelConnectionStatus.Controls.Add(this.panel2);
            this.panelConnectionStatus.Location = new System.Drawing.Point(33, 33);
            this.panelConnectionStatus.Name = "panelConnectionStatus";
            this.panelConnectionStatus.Size = new System.Drawing.Size(354, 223);
            this.panelConnectionStatus.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gray;
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.labelConnectionStatus);
            this.panel2.Location = new System.Drawing.Point(31, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(294, 165);
            this.panel2.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Gray;
            this.label6.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(178, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "Connection Status";
            // 
            // panelEngine
            // 
            this.panelEngine.BackgroundImage = global::FN2RCClient.Properties.Resources.legoPlateGrey1200x700;
            this.panelEngine.Controls.Add(this.labelTransmission);
            this.panelEngine.Controls.Add(this.rpmGuage1);
            this.panelEngine.Controls.Add(this.guagePowerControl1);
            this.panelEngine.Controls.Add(this.rpmGuage2);
            this.panelEngine.Location = new System.Drawing.Point(33, 287);
            this.panelEngine.Name = "panelEngine";
            this.panelEngine.Size = new System.Drawing.Size(832, 383);
            this.panelEngine.TabIndex = 10;
            // 
            // labelTransmission
            // 
            this.labelTransmission.BackColor = System.Drawing.Color.DarkGray;
            this.labelTransmission.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTransmission.ForeColor = System.Drawing.Color.White;
            this.labelTransmission.Location = new System.Drawing.Point(287, 318);
            this.labelTransmission.Name = "labelTransmission";
            this.labelTransmission.Size = new System.Drawing.Size(261, 32);
            this.labelTransmission.TabIndex = 8;
            this.labelTransmission.Text = "label8";
            this.labelTransmission.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rpmGuage1
            // 
            this.rpmGuage1.BackColor = System.Drawing.Color.Gray;
            this.rpmGuage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rpmGuage1.Location = new System.Drawing.Point(31, 62);
            this.rpmGuage1.Margin = new System.Windows.Forms.Padding(4);
            this.rpmGuage1.Name = "rpmGuage1";
            this.rpmGuage1.Size = new System.Drawing.Size(227, 285);
            this.rpmGuage1.TabIndex = 0;
            // 
            // guagePowerControl1
            // 
            this.guagePowerControl1.BackColor = System.Drawing.Color.Gray;
            this.guagePowerControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guagePowerControl1.Location = new System.Drawing.Point(289, 30);
            this.guagePowerControl1.Margin = new System.Windows.Forms.Padding(4);
            this.guagePowerControl1.Name = "guagePowerControl1";
            this.guagePowerControl1.Size = new System.Drawing.Size(255, 257);
            this.guagePowerControl1.TabIndex = 7;
            // 
            // rpmGuage2
            // 
            this.rpmGuage2.BackColor = System.Drawing.Color.Gray;
            this.rpmGuage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rpmGuage2.Location = new System.Drawing.Point(576, 62);
            this.rpmGuage2.Margin = new System.Windows.Forms.Padding(4);
            this.rpmGuage2.Name = "rpmGuage2";
            this.rpmGuage2.Size = new System.Drawing.Size(227, 285);
            this.rpmGuage2.TabIndex = 1;
            // 
            // panelSteering
            // 
            this.panelSteering.BackgroundImage = global::FN2RCClient.Properties.Resources.legoPlateLightGrey1200x700;
            this.panelSteering.Controls.Add(this.guageDualSteering1);
            this.panelSteering.Location = new System.Drawing.Point(897, 95);
            this.panelSteering.Name = "panelSteering";
            this.panelSteering.Size = new System.Drawing.Size(258, 512);
            this.panelSteering.TabIndex = 11;
            // 
            // guageDualSteering1
            // 
            this.guageDualSteering1.BackColor = System.Drawing.Color.Gray;
            this.guageDualSteering1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guageDualSteering1.Location = new System.Drawing.Point(33, 35);
            this.guageDualSteering1.Margin = new System.Windows.Forms.Padding(4);
            this.guageDualSteering1.Name = "guageDualSteering1";
            this.guageDualSteering1.Size = new System.Drawing.Size(193, 444);
            this.guageDualSteering1.TabIndex = 13;
            // 
            // panelControllerStatus
            // 
            this.panelControllerStatus.BackgroundImage = global::FN2RCClient.Properties.Resources.legoPlateLightGrey1200x700;
            this.panelControllerStatus.Controls.Add(this.panel1);
            this.panelControllerStatus.Location = new System.Drawing.Point(416, 33);
            this.panelControllerStatus.Name = "panelControllerStatus";
            this.panelControllerStatus.Size = new System.Drawing.Size(449, 223);
            this.panelControllerStatus.TabIndex = 12;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.guageBatteryDisplay);
            this.panel1.Controls.Add(this.guageCpuTemp);
            this.panel1.Controls.Add(this.guageCpuUsage);
            this.panel1.Location = new System.Drawing.Point(31, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(389, 165);
            this.panel1.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 18);
            this.label5.TabIndex = 5;
            this.label5.Text = "Controller Status";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(11, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 40);
            this.label4.TabIndex = 4;
            this.label4.Text = "Battery Voltage";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guageBatteryDisplay
            // 
            this.guageBatteryDisplay.BackColor = System.Drawing.Color.Black;
            this.guageBatteryDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guageBatteryDisplay.Location = new System.Drawing.Point(105, 34);
            this.guageBatteryDisplay.Margin = new System.Windows.Forms.Padding(4);
            this.guageBatteryDisplay.Name = "guageBatteryDisplay";
            this.guageBatteryDisplay.Size = new System.Drawing.Size(99, 58);
            this.guageBatteryDisplay.TabIndex = 3;
            // 
            // guageCpuTemp
            // 
            this.guageCpuTemp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.guageCpuTemp.BackColor = System.Drawing.Color.Gray;
            this.guageCpuTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guageCpuTemp.Location = new System.Drawing.Point(214, 6);
            this.guageCpuTemp.Margin = new System.Windows.Forms.Padding(4);
            this.guageCpuTemp.Name = "guageCpuTemp";
            this.guageCpuTemp.Size = new System.Drawing.Size(165, 100);
            this.guageCpuTemp.TabIndex = 2;
            // 
            // guageCpuUsage
            // 
            this.guageCpuUsage.BackColor = System.Drawing.Color.Gray;
            this.guageCpuUsage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guageCpuUsage.Location = new System.Drawing.Point(14, 107);
            this.guageCpuUsage.Margin = new System.Windows.Forms.Padding(4);
            this.guageCpuUsage.Name = "guageCpuUsage";
            this.guageCpuUsage.Size = new System.Drawing.Size(365, 54);
            this.guageCpuUsage.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::FN2RCClient.Properties.Resources.legoPlateGreen1200x700;
            this.ClientSize = new System.Drawing.Size(1184, 926);
            this.Controls.Add(this.panelControllerStatus);
            this.Controls.Add(this.panelSteering);
            this.Controls.Add(this.panelConnectionStatus);
            this.Controls.Add(this.groupBoxLogging);
            this.Controls.Add(this.panelEngine);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1200, 2000);
            this.MinimumSize = new System.Drawing.Size(1200, 965);
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.groupBoxLogging.ResumeLayout(false);
            this.groupBoxLogging.PerformLayout();
            this.panelConnectionStatus.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelEngine.ResumeLayout(false);
            this.panelSteering.ResumeLayout(false);
            this.panelControllerStatus.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labelConnectionStatus;
        private System.Windows.Forms.ListView listViewLogs;
        private System.Windows.Forms.ComboBox comboBoxLogLevel;
        private System.Windows.Forms.GroupBox groupBoxLogging;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxLogLevelRemote;
        private GuageRpmControl rpmGuage2;
        private GuageRpmControl rpmGuage1;
        private GuagePowerControl guagePowerControl1;
        private System.Windows.Forms.Panel panelConnectionStatus;
        private System.Windows.Forms.Panel panelEngine;
        private System.Windows.Forms.Panel panelSteering;
        private System.Windows.Forms.Panel panelControllerStatus;
        private GuageCpuUsage guageCpuUsage;
        private GuageDualSteering guageDualSteering1;
        private GuageCpuTemp guageCpuTemp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private GuageBatteryDisplay guageBatteryDisplay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxLogLevelWpi;
        private System.Windows.Forms.Label labelTransmission;
    }
}

