﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FN2RCClient
{
    public static class DoubleExtensionMethods
    {
        /// <summary>
        /// Compare two doubles to the specified precision
        /// </summary>
        public static bool AlmostEquals(this double value, double compare, double precision)
        {
            return (Math.Abs(value - compare) <= precision);
        }


        /// <summary>
        /// Compare two doubles to the specified precision
        /// </summary>
        public static bool AlmostEquals(this double? value, double compare, double precision)
        {
            if (!value.HasValue)
                return false;

            return (Math.Abs(value.Value - compare) <= precision);
        }


        /// <summary>
        /// Compare two doubles to the specified precision
        /// </summary>
        public static bool AlmostEquals(this double value, double? compare, double precision)
        {
            if (!compare.HasValue)
                return false;

            return (Math.Abs(value - compare.Value) <= precision);
        }

        /// <summary>
        /// Compare two doubles to the specified precision
        /// </summary>
        public static bool AlmostEquals(this double? value, double? compare, double precision)
        {
            if (!value.HasValue && !compare.HasValue)
                return true;
            if (value.HasValue != compare.HasValue)
                return false;

            return value.Value.AlmostEquals(compare.Value, precision);
        }


        /// <summary>
        /// Is same double precision number, tolerance double.Epsilon
        /// </summary>
        public static bool IsSameDouble(this double value, double compare)
        {
            return (Math.Abs(value - compare) < double.Epsilon);
        }


        /// <summary>
        /// Is same double precision number, tolerance double.Epsilon
        /// </summary>
        public static bool IsSameDouble(this double? value, double compare)
        {
            if (!value.HasValue)
                return false;

            return (Math.Abs(value.Value - compare) < double.Epsilon);
        }


        /// <summary>
        /// Is same double precision number, tolerance double.Epsilon
        /// </summary>
        public static bool IsSameDouble(this double value, double? compare)
        {
            if (!compare.HasValue)
                return false;

            return (Math.Abs(value - compare.Value) < double.Epsilon);
        }


        /// <summary>
        /// Is same double precision number, tolerance double.Epsilon
        /// </summary>
        public static bool IsSameDouble(this double? value, double? compare)
        {
            if (!value.HasValue && !compare.HasValue)
                return true;
            if (value.HasValue != compare.HasValue)
                return false;

            return value.Value.IsSameDouble(compare.Value);
        }


        /// <summary>
        /// Is 0.0 double precision
        /// </summary>
        public static bool IsZero(this double value)
        {
            return IsSameDouble(value, 0.0);
        }


        /// <summary>
        /// Is 0.0 double precision
        /// </summary>
        public static bool IsZero(this double? value)
        {
            return IsSameDouble(value, 0.0);
        }
    }
}
