﻿namespace FN2RCClient
{
    partial class GuageBatteryDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.glgControlGuage = new GenLogic.GlgControl();
            this.SuspendLayout();
            // 
            // glgControlGuage
            // 
            this.glgControlGuage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.glgControlGuage.BackColor = System.Drawing.Color.DimGray;
            this.glgControlGuage.DrawingFile = "";
            this.glgControlGuage.DrawingObject = null;
            this.glgControlGuage.DrawingURL = "";
            this.glgControlGuage.Enabled = false;
            this.glgControlGuage.HierarchyEnabled = true;
            this.glgControlGuage.Location = new System.Drawing.Point(3, 3);
            this.glgControlGuage.MinimumSize = new System.Drawing.Size(5, 5);
            this.glgControlGuage.Name = "glgControlGuage";
            this.glgControlGuage.SelectEnabled = true;
            this.glgControlGuage.Size = new System.Drawing.Size(144, 83);
            this.glgControlGuage.TabIndex = 8;
            this.glgControlGuage.Text = "glgControl1";
            this.glgControlGuage.Trace2Enabled = false;
            this.glgControlGuage.TraceEnabled = false;
            // 
            // GuageBatteryDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.glgControlGuage);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GuageBatteryDisplay";
            this.Size = new System.Drawing.Size(150, 89);
            this.ResumeLayout(false);

        }

        #endregion

        private GenLogic.GlgControl glgControlGuage;
    }
}
