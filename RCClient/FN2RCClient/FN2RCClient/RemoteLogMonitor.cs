﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace FN2RCClient
{
    /// <summary>
    /// Remote Logging event args
    /// </summary>
    public class RemoteLogEventArgs : EventArgs
    {
        public RemoteLogEventArgs()
        {
            Thread = -1;
            Sender = "";
            Level = LogLevel.All;
            Function = "";
            Time = 0;
            Data = null;
        }
        
        public int Thread { get; set; }
        public object Sender { get; set; }
        public LogLevel Level { get; set; }
        public string Function { get; set; }
        public long Time { get; set; }
        public object Data { get; set; }
    }
    //
    public delegate void RemoteLogEventDelegate(object sender, RemoteLogEventArgs e);



    class RemoteLogMonitor
    {
        public event RemoteLogEventDelegate RemoteLogReceived;
        public event LogEventDelegate Log;


        /// <summary>
        /// Start monitor
        /// </summary>
        public async Task StartMonitorAsync()
        {
            await StopMonitorAsync();

            MonitorCancelTokenSource = new CancellationTokenSource();
            MonitorRunTask = RunMonitor(MonitorCancelTokenSource.Token);
        }


        /// <summary>
        /// Stop Monitor
        /// </summary>
        public async Task StopMonitorAsync()
        {
            if (MonitorCancelTokenSource != null)
            {
                MonitorCancelTokenSource.Cancel();

                if (MonitorRunTask != null)
                    await MonitorRunTask;

                MonitorCancelTokenSource = null;
                MonitorRunTask = null;
            }
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public RemoteLogMonitor()
        {

        }


        // Monitor task
        CancellationTokenSource MonitorCancelTokenSource { get; set; }
        Task MonitorRunTask { get; set; }


        /// <summary>
        /// Monitor run function
        /// </summary>
        protected async Task RunMonitor(CancellationToken cancelToken)
        {
            try
            {
                //  TODO - ports into settings
                using (var udpClient = new UdpClient(48885))
                {
                    try
                    {
                        udpClient.JoinMulticastGroup(IPAddress.Parse("234.5.6.7"));
                        //System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                        //sw.Start();

                        while (!cancelToken.IsCancellationRequested)
                        {
                            try
                            {
                                //  wait for the next read, and then split it into the command and the arguments
                                var parseReceived = Encoding.ASCII.GetString((await udpClient.ReceiveAsync()).Buffer).Split('?');

                                if (parseReceived.Count() == 2)
                                {
                                    //  see if this is recognized command
                                    switch (parseReceived[0])
                                    {
                                        case "log":
                                            //  parse the arguments
                                            var responseArgs = HttpUtility.ParseQueryString(parseReceived[1]);
                                            var logString = responseArgs.Get("log");
                                            if (logString != null)
                                            {
                                                var log = JsonConvert.DeserializeObject<RemoteLogEventArgs>(logString);
                                                RemoteLogReceived?.Invoke(this, log);
                                            }
                                            break;

                                        default:
                                            Log?.Invoke(this, new LogEventArgs(this, "RunMonitor", $"Received invalid remote log: {parseReceived}.", LogLevel.WARN));
                                            break;
                                    }
                                }
                            }
                            catch (Exception exc)
                            {
                                Log?.Invoke(this, new LogEventArgs(this, "RunMonitor",exc, LogLevel.ERROR));
                            }
                        }
                    }
                    catch ( OperationCanceledException)
                    { }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Log?.Invoke(this, new LogEventArgs(this, "RunMonitor", e, LogLevel.FATAL));
            }

        }
    }
}
