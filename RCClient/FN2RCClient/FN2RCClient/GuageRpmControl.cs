﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FN2RCClient
{
    public partial class GuageRpmControl : UserControl
    {
        public GuageRpmControl()
        {
            InitializeComponent();
#if EDITUI
            glgControlGuage.DrawingFile = Path.Combine("C:/Users/grahambriggs/source/lbp_frankenNXT20/RCClient/FN2RCClient/FN2RCClient/Resources", "rpmguage.g");
#else
            glgControlGuage.DrawingFile = "./Resources/rpmguage.g";
#endif
            UpdateControl(0.0);
        }

        

        public void SetLabel(string label)
        {
            labelLabel.Text = label;
        }

        protected double LastValue = -1.0;
        protected double UpdateTolerance = 1.0;

        public void UpdateControl(double rpm)
        {
            if (!LastValue.AlmostEquals(rpm, UpdateTolerance))
            {

                glgControlGuage.SetDResource("Value", Math.Abs(rpm));
                glgControlGuage.UpdateGlg();
                LastValue = rpm;
            }
            labelValue.Text = $"{rpm.ToString("N2"),5}";
        }
    }

}
