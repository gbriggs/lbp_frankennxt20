﻿namespace FN2RCClient
{
    partial class GuageCpuTemp
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTag = new System.Windows.Forms.Label();
            this.labelValue = new System.Windows.Forms.Label();
            this.glgControlGuage = new GenLogic.GlgControl();
            this.SuspendLayout();
            // 
            // labelTag
            // 
            this.labelTag.BackColor = System.Drawing.Color.Gray;
            this.labelTag.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTag.ForeColor = System.Drawing.Color.White;
            this.labelTag.Location = new System.Drawing.Point(5, 3);
            this.labelTag.Name = "labelTag";
            this.labelTag.Size = new System.Drawing.Size(67, 26);
            this.labelTag.TabIndex = 9;
            this.labelTag.Text = "CPU Temp";
            this.labelTag.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelValue
            // 
            this.labelValue.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValue.ForeColor = System.Drawing.Color.White;
            this.labelValue.Location = new System.Drawing.Point(3, 30);
            this.labelValue.Name = "labelValue";
            this.labelValue.Size = new System.Drawing.Size(69, 24);
            this.labelValue.TabIndex = 8;
            this.labelValue.Text = "sdf";
            this.labelValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // glgControlGuage
            // 
            this.glgControlGuage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.glgControlGuage.BackColor = System.Drawing.Color.DimGray;
            this.glgControlGuage.DrawingFile = "";
            this.glgControlGuage.DrawingObject = null;
            this.glgControlGuage.DrawingURL = "";
            this.glgControlGuage.Enabled = false;
            this.glgControlGuage.HierarchyEnabled = true;
            this.glgControlGuage.Location = new System.Drawing.Point(78, 3);
            this.glgControlGuage.MinimumSize = new System.Drawing.Size(5, 5);
            this.glgControlGuage.Name = "glgControlGuage";
            this.glgControlGuage.SelectEnabled = true;
            this.glgControlGuage.Size = new System.Drawing.Size(160, 198);
            this.glgControlGuage.TabIndex = 7;
            this.glgControlGuage.Text = "glgControl1";
            this.glgControlGuage.Trace2Enabled = false;
            this.glgControlGuage.TraceEnabled = false;
            // 
            // GuageCpuTemp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.labelValue);
            this.Controls.Add(this.glgControlGuage);
            this.Controls.Add(this.labelTag);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GuageCpuTemp";
            this.Size = new System.Drawing.Size(241, 204);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTag;
        private System.Windows.Forms.Label labelValue;
        private GenLogic.GlgControl glgControlGuage;
    }
}
