﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;



namespace FN2RCClient
{
   


    public partial class Form1 : Form
    {
           
     
        //ConnectionForm ConnectForm;
     
        RobotStatusMonitor StatusMonitor;

        
        /// <summary>
        /// Constructor
        /// </summary>
        public Form1()
        {
            InitializeComponent();
         
            //  create the logging
            Logger = new Logging();
            Logger.LoggedEvents += OnLoggedEvents;
            BeginLogging();

            //  setup the UI on this form
            SetupUi();

            //  start the status monitor
            StatusMonitor = new RobotStatusMonitor();
            StatusMonitor.Log += Logger.AddLog;
            BeginStatusMonitor();

        }

        Logging Logger;

        //  Set the desired log level
        LogLevel LogLevelDisplay { get; set; }


        /// <summary>
        /// Setup the UI for this form
        /// </summary>
        void SetupUi()
        {
            //  log display list view
            listViewLogs.View = View.Details;
            listViewLogs.Columns.Add("", listViewLogs.Width, HorizontalAlignment.Left);
            listViewLogs.HeaderStyle = ColumnHeaderStyle.None;
            listViewLogs.Resize += listViewLogs_Resize;

            // log settings combo box
            for (LogLevel nextLevel = LogLevel.VERBOSE; nextLevel <= LogLevel.OFF; nextLevel++)
            {
                var nextItem = comboBoxLogLevel.Items.Add(nextLevel);
            }
            comboBoxLogLevel.SelectedItem = LogLevel.INFO;

            //  remote log settings combo box
            comboBoxLogLevelRemote.DataSource = Enum.GetValues(typeof(LogLevelRemote));
            comboBoxLogLevelRemote.SelectedItem = LogLevelRemote.INFO;
            comboBoxLogLevelRemote.SelectedIndexChanged += comboBoxLogLevelRemote_SelectedIndexChanged;

            comboBoxLogLevelWpi.DataSource = Enum.GetValues(typeof(LogLevelRemote));
            comboBoxLogLevelWpi.SelectedItem = LogLevelRemote.INFO;
            comboBoxLogLevelWpi.SelectedIndexChanged += comboBoxLogLevelWpi_SelectedIndexChanged;

            rpmGuage1.SetLabel("Motor 1 RPM");
            rpmGuage2.SetLabel("Motor 2 RPM");
        }

       

        private async void BeginStatusMonitor()
        {
            StatusMonitor.RobotConnectionStatusChanged += OnRobotConnectionStatusChanged; 

            StatusMonitor.RobotStatusEnginePowerChanged += OnRobotStatusEnginePowerChanged; 
            StatusMonitor.RobotStatusMotor1RpmChanged += OnRobotStatusMotor1RpmChanged;
            StatusMonitor.RobotStatusMotor2RpmChanged += OnRobotStatusMotor2RpmChanged;
            StatusMonitor.RobotStatusGearChanged += OnRobotStatusGearChanged; ;
            StatusMonitor.RobotStatusFrontSteeringChanged += OnRobotStatusSteeringChanged;
            StatusMonitor.RobotStatusRearSteeringChanged += OnRobotStatusSteeringChanged;

            StatusMonitor.RobotControllerStatusChanged += OnRobotControllerStatusChanged; ; ;
            

            await StatusMonitor.StartMonitorAsync();
        }

      

        private void OnRobotControllerStatusChanged(object sender, RobotControllerStatusEventArgs e)
        {
            guageBatteryDisplay.UpdateControl(e.Status.MainBattVoltage);
            guageCpuTemp.UpdateControl(e.Status.CpuTemp);
            guageCpuUsage.UpdateControl(e.Status.CpuUsage);
        }


        private void OnRobotStatusSteeringChanged(object sender, RobotEngineRoomStatusEventArgs e)
        {
            guageDualSteering1.UpdateControl(e.Status.FrontSteering, e.Status.RearSteering);  
        }


        private void OnRobotConnectionStatusChanged(object sender, RobotConnectionStatusEventArgs e)
        {   
            string statusString = $"eth0:  {e.Status.Eth0Connection()}\n";
            statusString += $"wlan0: {e.Status.WLan0Connection()}\n";
            statusString += $"ping speed: {e.Status.PingSpeed.TotalMilliseconds.ToString("N3")}.";
            labelConnectionStatus.Text = statusString;
        }

      

        private void OnRobotStatusEnginePowerChanged(object sender, RobotEngineRoomStatusEventArgs e)
        {
            guagePowerControl1.UpdateControl(e.Status);
        }

        private void OnRobotStatusMotor2RpmChanged(object sender, RobotEngineRoomStatusEventArgs e)
        {
            rpmGuage2.UpdateControl(e.Status.EngineMotor2RPM);
        }

        private void OnRobotStatusMotor1RpmChanged(object sender, RobotEngineRoomStatusEventArgs e)
        {
            rpmGuage1.UpdateControl(e.Status.EngineMotor1RPM);
        }

        int LastDisplayedGear = -9;
        int LastDisplayedDirection = -9;

        private void OnRobotStatusGearChanged(object sender, RobotEngineRoomStatusEventArgs e)
        {
            if (e.Status.Gear != LastDisplayedGear || e.Status.Direction != LastDisplayedDirection)
            {
                string dirString = e.Status.Direction == 1 ? "Forward" : "Reverse";
                string gearString = "";
                switch (e.Status.Gear)
                {
                    case 1:
                        gearString = "Low";
                        break;
                    case 2:
                        gearString = "High";
                        break;
                    case 0:
                        gearString = "Neutral";
                        break;
                    case -1:
                        gearString = "Switching";
                        break;
                }

                labelTransmission.Text = $"{dirString} {gearString}";
                
                if ( e.Status.Direction != LastDisplayedDirection )
                {
                    labelTransmission.ForeColor = e.Status.Direction == 1 ?  Color.Green : Color.Orange;
                }
                LastDisplayedDirection = e.Status.Direction;
                LastDisplayedGear = e.Status.Gear;
            }
        }



        private async void BeginLogging()
        {
            await Logger.StartLogging();
        }


        



        private void button1_Click(object sender, EventArgs e)
        {

        }


        private async void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ( ! RobotConnection.IpAddressIsValid )
                {
                    OnProgramLog(this, new LogEventArgs(this, "Form1_KeyDown", $"Robot IP address is not known.", LogLevel.WARN));
                    return;
                }

                OnProgramLog(this, new LogEventArgs(this, "Form1_KeyDown", $"Start process key down {e.KeyValue}.", LogLevel.DEBUG));

                var response = await TcpRequest.GetTcpResponse(RobotConnection.IpAddress, RobotConnection.ComPort, $"keyboard?key={e.KeyValue}");
                if (response.CheckFnxtResponse())
                {
                    OnProgramLog(this, new LogEventArgs(this, "Form1_KeyDown", $"Finish process key down {e.KeyValue}.", LogLevel.TRACE));
                }
                else
                {
                    OnProgramLog(this, new LogEventArgs(this, "Form1_KeyDown", $"Finish process key down {e.KeyValue}. Received an invalid response.", LogLevel.ERROR));
                }
            }
            catch (Exception ex)
            {
                OnProgramLog(this, new LogEventArgs(this, "Form1_KeyDown", ex , LogLevel.ERROR));
            }
        }
    }
}
