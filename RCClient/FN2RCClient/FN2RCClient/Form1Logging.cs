﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FN2RCClient
{
    public partial class Form1
    {

        /// <summary>
        /// Sub set of log levels for UI choices of remote logging
        /// </summary>
        public enum LogLevelRemote
        {
            TRACE = 2,
            DEBUG,
            INFO,
            WARN,
            ERROR,
            FATAL,
            USER,
            OFF,
        }



        /// <summary>
        /// Program log function, will queue the log up for processing
        /// </summary>
        public void OnProgramLog(object sender, LogEventArgs e)
        {
            Logger.AddLog(e);
        }


        /// <summary>
        /// Logging function, update the UI with the log and send it to the log4 appenders
        /// </summary>
        public void OnLoggedEvents(object sender, IEnumerable<LogEventArgs> logs)
        {
            var logsToDisplay = logs.Where(x => x.Level >= LogLevelDisplay);

            if (logsToDisplay.Count() > 0)
            {
                listViewLogs.BeginUpdate();

                foreach (var nextLog in logsToDisplay)
                {
                    if (nextLog.Level >= LogLevelDisplay)
                    {
                        var item = listViewLogs.Items.Insert(0, nextLog.FormatLogForConsole());
                        item.ForeColor = nextLog.Level.LogColour();
                        item.BackColor = nextLog.Level.BackgrondColour(nextLog.Remote);
                    }
                }

                while (listViewLogs.Items.Count > 500)
                    listViewLogs.Items.RemoveAt(listViewLogs.Items.Count - 1);

                listViewLogs.EndUpdate();
            }
        }



        private void listViewLogs_Resize(object sender, EventArgs e)
        {

            listViewLogs.Columns[0].Width = listViewLogs.Width + 10;

        }


        private void comboBoxLogLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            LogLevelDisplay = (LogLevel)comboBoxLogLevel.SelectedItem;
        }


        private async void comboBoxLogLevelRemote_SelectedIndexChanged(object sender, EventArgs e)
        {
            var level = (LogLevelRemote)comboBoxLogLevelRemote.SelectedItem;
            var wpiLevel = (LogLevelRemote)comboBoxLogLevelWpi.SelectedItem;
            if ( level > wpiLevel)
            {
                comboBoxLogLevelRemote.SelectedIndexChanged -= comboBoxLogLevelRemote_SelectedIndexChanged;
                comboBoxLogLevelRemote.SelectedItem = wpiLevel;
                comboBoxLogLevelRemote.SelectedIndexChanged += comboBoxLogLevelRemote_SelectedIndexChanged;
                OnProgramLog(this, new LogEventArgs(this, "comboBoxRemoteLogLevel_SelectedIndexChanged", $"Level {level} can not be greater than wpi level {wpiLevel}.", LogLevel.WARN));
                return;
            }
            try
            {
                if (!RobotConnection.IpAddressIsValid)
                {
                    OnProgramLog(this, new LogEventArgs(this, "comboBoxRemoteLogLevel_SelectedIndexChanged", $"Robot IP address is not known.", LogLevel.WARN));
                    return;
                }

                OnProgramLog(this, new LogEventArgs(this, "comboBoxRemoteLogLevel_SelectedIndexChanged", $"Setting remote log level {level}.", LogLevel.DEBUG));
                var response = await TcpRequest.GetTcpResponse(RobotConnection.IpAddress, RobotConnection.ComPort, $"loglevel?object=a&level={(int)level}");
                if (!response.CheckFnxtResponse())
                {
                    OnProgramLog(this, new LogEventArgs(this, "comboBoxRemoteLogLevel_SelectedIndexChanged", $"Received an invalid response {response}.", LogLevel.ERROR));
                }
            }
            catch (Exception ex)
            {
                OnProgramLog(this, new LogEventArgs(this, "comboBoxRemoteLogLevel_SelectedIndexChanged", ex, LogLevel.ERROR));
            }
        }

        private async void comboBoxLogLevelWpi_SelectedIndexChanged(object sender, EventArgs e)
        {
            var level = (LogLevelRemote)comboBoxLogLevelWpi.SelectedItem;
            try
            {
                if (!RobotConnection.IpAddressIsValid)
                {
                    OnProgramLog(this, new LogEventArgs(this, "comboBoxLogLevelWpi_SelectedIndexChanged", $"Robot IP address is not known.", LogLevel.WARN));
                    return;
                }

                OnProgramLog(this, new LogEventArgs(this, "comboBoxLogLevelWpi_SelectedIndexChanged", $"Setting remote wpi log level {level}.", LogLevel.DEBUG));
                var response = await TcpRequest.GetTcpResponse(RobotConnection.IpAddress, RobotConnection.ComPort, $"loglevel?object=w&level={(int)level}");
                if (!response.CheckFnxtResponse())
                {
                    OnProgramLog(this, new LogEventArgs(this, "comboBoxLogLevelWpi_SelectedIndexChanged", $"Received an invalid response {response}.", LogLevel.ERROR));
                }
                else if ( level < (LogLevelRemote)comboBoxLogLevelRemote.SelectedItem)
                {
                    comboBoxLogLevelRemote.SelectedIndexChanged -= comboBoxLogLevelRemote_SelectedIndexChanged;
                    comboBoxLogLevelRemote.SelectedItem = level;
                    comboBoxLogLevelRemote.SelectedIndexChanged += comboBoxLogLevelRemote_SelectedIndexChanged;
                }
            }
            catch (Exception ex)
            {
                OnProgramLog(this, new LogEventArgs(this, "comboBoxLogLevelWpi_SelectedIndexChanged", ex, LogLevel.ERROR));
            }
        }


        //private async void LogLots()
        //{
        //    for (int i = 0; i < 100; i++)
        //    {
        //        await Task.Delay(TimeSpan.FromSeconds(1));

        //        OnProgramLog(this, new LogEventArgs(this, "VerboseLog", "Verbose logging string", LogLevel.VERBOSE));
        //        OnProgramLog(this, new LogEventArgs(this, "TraceLogFunction", "Trace logging string", LogLevel.TRACE));
        //        OnProgramLog(this, new LogEventArgs(this, "DebugLogFunction", "Debug logging string.", LogLevel.DEBUG));
        //        OnProgramLog(this, new LogEventArgs(this, "InfoLogFunction", "Info logging string", LogLevel.INFO));
        //        OnProgramLog(this, new LogEventArgs(this, "UserLogFunction", "User logging string", LogLevel.USER));
        //        OnProgramLog(this, new LogEventArgs(this, "WarnLogFunction", "Warning logging string.", LogLevel.WARN));
        //        OnProgramLog(this, new LogEventArgs(this, "ErrorLogFunction", "Error logging string.", LogLevel.ERROR));
        //        OnProgramLog(this, new LogEventArgs(this, "FatalLogFunction", "Fatal logging string", LogLevel.FATAL));
        //    }

        //}



    }
}
