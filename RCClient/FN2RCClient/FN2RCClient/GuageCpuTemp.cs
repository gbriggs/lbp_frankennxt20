﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FN2RCClient
{
    public partial class GuageCpuTemp : UserControl
    {
        public GuageCpuTemp()
        {
            InitializeComponent();

#if EDITUI
            glgControlGuage.DrawingFile = Path.Combine("C:/Users/grahambriggs/source/lbp_frankenNXT20/RCClient/FN2RCClient/FN2RCClient/Resources", "cputempguage.g");
#else
            glgControlGuage.DrawingFile = "./Resources/cputempguage.g";
#endif
            labelTag.Text = "CPU Temp";
            labelTag.BackColor = Color.Transparent;
            UpdateControl(0.0);
        }

        protected double LastValue = -1.0;
        protected double UpdateTolerance = 1.0;

        public void UpdateControl(double cpuTemp)
        {
            if (!LastValue.AlmostEquals(cpuTemp, UpdateTolerance))
            {
                var value = (cpuTemp - 15) / .7;
                glgControlGuage.SetDResource("Value", value);
                glgControlGuage.UpdateGlg();
                LastValue = cpuTemp;
            }
            labelValue.Text = $"{cpuTemp.ToString("N1"),3}°C";
        }

    }
}
