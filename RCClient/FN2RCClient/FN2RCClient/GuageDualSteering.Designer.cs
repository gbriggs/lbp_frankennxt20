﻿namespace FN2RCClient
{
    partial class GuageDualSteering
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.glgControlGuage = new GenLogic.GlgControl();
            this.labelFront = new System.Windows.Forms.Label();
            this.labelRear = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // glgControlGuage
            // 
            this.glgControlGuage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.glgControlGuage.BackColor = System.Drawing.Color.LightGray;
            this.glgControlGuage.DrawingFile = "";
            this.glgControlGuage.DrawingObject = null;
            this.glgControlGuage.DrawingURL = "";
            this.glgControlGuage.Enabled = false;
            this.glgControlGuage.HierarchyEnabled = true;
            this.glgControlGuage.Location = new System.Drawing.Point(13, 41);
            this.glgControlGuage.MinimumSize = new System.Drawing.Size(5, 5);
            this.glgControlGuage.Name = "glgControlGuage";
            this.glgControlGuage.SelectEnabled = true;
            this.glgControlGuage.Size = new System.Drawing.Size(168, 357);
            this.glgControlGuage.TabIndex = 6;
            this.glgControlGuage.Text = "glgControl1";
            this.glgControlGuage.Trace2Enabled = false;
            this.glgControlGuage.TraceEnabled = false;
            // 
            // labelFront
            // 
            this.labelFront.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFront.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFront.ForeColor = System.Drawing.Color.White;
            this.labelFront.Location = new System.Drawing.Point(5, 0);
            this.labelFront.Name = "labelFront";
            this.labelFront.Size = new System.Drawing.Size(184, 38);
            this.labelFront.TabIndex = 7;
            this.labelFront.Text = "Front";
            this.labelFront.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRear
            // 
            this.labelRear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRear.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRear.ForeColor = System.Drawing.Color.White;
            this.labelRear.Location = new System.Drawing.Point(3, 407);
            this.labelRear.Name = "labelRear";
            this.labelRear.Size = new System.Drawing.Size(187, 31);
            this.labelRear.TabIndex = 8;
            this.labelRear.Text = "Rear";
            this.labelRear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GuageDualSteering
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.labelRear);
            this.Controls.Add(this.labelFront);
            this.Controls.Add(this.glgControlGuage);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GuageDualSteering";
            this.Size = new System.Drawing.Size(193, 444);
            this.ResumeLayout(false);

        }

        #endregion

        private GenLogic.GlgControl glgControlGuage;
        private System.Windows.Forms.Label labelFront;
        private System.Windows.Forms.Label labelRear;
    }
}
