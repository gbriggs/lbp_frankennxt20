// rp-mcp3008 Reads analogue values through MCP3008 chip


#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <math.h>

#include "WiringPiExtensionApi.h"


#define SECONDMICRO (1000000)
#define MSMICRO (1000)

#define MCP3008_BASE 100
#define SPI_CHAN 0

#define PCA9685_BASE 200

using namespace std;


//  Adafruit Motor Hat
int AFHBase = PCA9685_BASE;
//   M1 on motor hat - PWM 08 / Input 10, 09
int M1in1 = AFHBase + 10;
int M1in2 = AFHBase + 9;
int M1pwm = AFHBase + 8;
//   M2 on motor hat - PWM 13 / Input 11, 12
int M2in1 = AFHBase + 11;
int M2in2 = AFHBase + 12;
int M2pwm = AFHBase + 13;
//   M3 on motor hat - PWM 02 / Input 04, 03
int M3in1 = AFHBase + 4;
int M3in2 = AFHBase + 3;
int M3pwm = AFHBase + 2;
//   M4 on motor hat - PWM 207 / Input 205, 206
int M4in1 = AFHBase + 5;
int M4in2 = AFHBase + 6;
int M4pwm = AFHBase + 7;

//  Extra Motor Controller
//  channel A
int MAin1 = 22;
int MAin2 = 18;
int MApwm = AFHBase + 0;
//  channel B
int MBin1 = AFHBase + 1;
int MBin2 = AFHBase + 14;
int MBpwm = AFHBase + 15;

//  Six Motor Connections
int Motor1;
int Motor2;
int Motor3;
int Motor4;
int MotorA;
int MotorB;

//  FrankenNXT Motor Encoder Wiring
int Motor1EncoderA = 31;
int Motor1EncoderB = 29;
int Motor2EncoderA = 37;
int Motor2EncoderB = 33;
int Motor3EncoderA = 38;
int Motor3EncoderB = 36;
int Motor4EncoderA = 32;
int Motor4EncoderB = 12;	
int MotorAEncoderA = 11;
int MotorAEncoderB = 7;
int MotorBEncoderA = 15;
int MotorBEncoderB = 13;

//  FrankenNXT2.0 Configuration
int EngineOne;
int EngineTwo;
int Transmission;
int SteeringForward;
int SteeringBack;
int ExtraMotor;

int KeypadButton3Pin = 16;
int LedPin = 35;


void CreateMotors()
{
	//  Motors on Motor Hat HBridges

	//  Motor 1 
	//   HBridge MotorA 
	Motor1 = MotorWithRotaryEncoderCreate(MAin1, MAin2, MApwm, Motor1EncoderA, Motor1EncoderB, -1, 180, 0x00);
	MotorWithRotaryEncoderStop(Motor1);
	
	
	//  Motor 2 
	//   HBridge MotorB
	Motor2 = MotorWithRotaryEncoderCreate(MBin1, MBin2, MBpwm, Motor2EncoderA, Motor2EncoderB, -1, 180, 0x00);
	MotorWithRotaryEncoderStop(Motor2);
	
	//  Motor 3 
	//   M1 on motor hat
	Motor3 = MotorWithRotaryEncoderCreate(M1in1, M1in2, M1pwm, Motor3EncoderA, Motor3EncoderB, -1, 180, 0x00);
	MotorWithRotaryEncoderStop(Motor3);
	
	//  Motor 4 
	//   M2 on motor hat
	Motor4 = MotorWithRotaryEncoderCreate(M2in1, M2in2, M2pwm, Motor4EncoderA, Motor4EncoderB, -1, 180, 0x00);
	MotorWithRotaryEncoderStop(Motor4);
	
	//  Motor A 
	//   M3 on motor hat
	MotorA = MotorWithRotaryEncoderCreate(M3in1, M3in2, M3pwm, MotorAEncoderA, MotorAEncoderB, -1, 180, 0x00);
	MotorWithRotaryEncoderStop(MotorA);
	
	//  Motor B 
	//   M4 on motor hat
	MotorB = MotorWithRotaryEncoderCreate(M4in1, M4in2, M4pwm, MotorBEncoderA, MotorBEncoderB, -1, 180, 0x00);
	MotorWithRotaryEncoderStop(MotorB);
	

}


//  Get switch number from adc value
int GetAdcSwitchValue(int adcValue)
{
	cout << "ADC " << adcValue << endl;
	
	if (adcValue > 1000)
		return 1;
	else if (adcValue > 375)
		return 2;
	else if (adcValue > 100)
		return 4;
	else
		return 0;
	
	
	
}


int GetKeypadButtonPress()
{
	// Keypad Buttons
	//  Left = SW4
	//  Center = SW3
	//  Right = SW2
	//  Lower = SW1
	
	int adcSwitch = GetAdcSwitchValue(AnalogRead(MCP3008_BASE + 1));
	switch (adcSwitch)
	{
	case 0:
		break;
	default:
		return adcSwitch;
	}
		
	int mainSwitch = DigitalRead(KeypadButton3Pin);
	if (mainSwitch == 1)
		return 3;
		
	usleep(100*MSMICRO);
	
	return 0;
}

void FlashLed()
{
	int flashState = DigitalRead(LedPin);
	if (flashState == 0)
		flashState = 1;
	else
		flashState = 0;
		
	DigitalWrite(LedPin, flashState);
}

//  Wait for button
bool WaitForButton(int button, int timeoutSeconds)
{
	int buttonTimeout = timeoutSeconds * 1000;
	
	while (GetKeypadButtonPress() != button && buttonTimeout > 0)
	{
		usleep(100*MSMICRO);
		FlashLed();
		buttonTimeout -= 100;
		if (fmod(buttonTimeout, 1000) == 0.0)
			cout << "Waiting for button " << button << " " << (int)(buttonTimeout / 1000) << endl;
	}
	
	if (buttonTimeout == 0)
		return false;
	
	DigitalWrite(LedPin, 0);
	
	return true;
}


bool TestButtons()
{
	cout << "Press left arrow." << endl;
	if (!WaitForButton(4, 60))
		return false;
	cout << "Left arrow pressed." << endl;
	
	cout << "Press center button." << endl;
	if (!WaitForButton(3, 60))
		return false;
	cout << "Center button pressed." << endl;
	
	cout << "Press right arrow." << endl;
	if (!WaitForButton(2, 60))
		return false;
	cout << "Right arrow pressed" << endl;
	
	cout << "Press lower button." << endl;
	if (!WaitForButton(1, 60))
		return false;
	cout << "Lower button pressed." << endl;
	
	return true;
}



//  Prompt for start text on next motor
bool ButtonToAdvance = true;
bool GotoNextMotor()
{
	if (ButtonToAdvance)
	{
		cout << " Press button 1 to start." << endl;
		
		if (!WaitForButton(3, 60))	
		{
			return false;
		}
		return true;
	}
	else
	{
		cout << endl;
		
		usleep(2*SECONDMICRO);
		
		return true;
	}
}


//  Motor test function
void TestMotor(int motorIndex)
{
	DigitalWrite(LedPin, 1);
	
	cout << "Run motor " << motorIndex << " forward." << endl;
	MotorWithRotaryEncoderRun(motorIndex, .5);
	usleep(2*SECONDMICRO);
	MotorWithRotaryEncoderBrake(motorIndex, 1.0);
	usleep(2*SECONDMICRO);
	
	cout << "Run motor " << motorIndex << " backwards." << endl;
	MotorWithRotaryEncoderRun(motorIndex, -.5);
	usleep(2*SECONDMICRO);
	MotorWithRotaryEncoderBrake(motorIndex, 1.0);
	usleep(2*SECONDMICRO);
	
	cout << "Motor " << motorIndex << " reset count." << endl;
	MotorWithRotaryEncoderRun(motorIndex, 0.0);
	MotorWithRotaryEncoderResetCount(motorIndex, 0);
	
	cout << "Turn motor " << motorIndex << " 2 rotations forward." << endl;
	MotorWithRotaryEncoderTurnBy(motorIndex, 2.0, .25);
	int timeRemaining = 3*SECONDMICRO;
	while (MotorWithRotaryEncoderGetCircle(motorIndex) < 1.9 && timeRemaining > 0)
	{
		usleep(100*MSMICRO);
		timeRemaining -= 100*MSMICRO;
	}
	usleep(2*SECONDMICRO);
	
	cout << "Turn motor " << motorIndex << " 2 rotations backwards." << endl;
	MotorWithRotaryEncoderTurnBy(motorIndex, -2.0, .25);
	timeRemaining = 3*SECONDMICRO;
	while (MotorWithRotaryEncoderGetCircle(motorIndex) > .1 && timeRemaining > 0)
	{
		usleep(100*MSMICRO);
		timeRemaining -= 100*MSMICRO;
	}
		
	usleep(2*SECONDMICRO);
	
	cout << "Motor " << motorIndex << " off." << endl;
	MotorWithRotaryEncoderRun(motorIndex, 0.0);
	
	DigitalWrite(LedPin, 0);
}

bool TestMotors()
{
	cout << "Testing motor 4."; 	
	if (!GotoNextMotor())	
	{
		return false;
	}
	TestMotor(Motor4);
		
	cout << "Testing motor 3."; 
	if (!GotoNextMotor())	
	{
		return false;
	}
	TestMotor(Motor3);
		
	cout << "Testing motor 2."; 
	if (!GotoNextMotor())	
	{
		return false;
	}
	TestMotor(Motor2);
		
	cout << "Testing motor 1."; 
	if (!GotoNextMotor())	
	{
		return false;
	}
	TestMotor(Motor1);
	
	cout << "Testing motor A."; 
	if (!GotoNextMotor())	
	{
		return false;
	}
	TestMotor(MotorA);
	
	cout << "Testing motor B."; 
	if (!GotoNextMotor())	
	{
		return false;
	}
	TestMotor(MotorB);
	
	return true;
}

bool TestLights()
{	
	DigitalWrite(LedPin, 1);
	usleep(1*SECONDMICRO);
	DigitalWrite(LedPin, 0);
	usleep(1*SECONDMICRO);
	DigitalWrite(LedPin, 1);
	usleep(1*SECONDMICRO);
	DigitalWrite(LedPin, 0);
	usleep(1*SECONDMICRO);
	DigitalWrite(LedPin, 1);
	usleep(1*SECONDMICRO);
	DigitalWrite(LedPin, 0);

	return true;
}

//  main
int main(int argc, char *argv[])
{
		
	//  setup WiringPi for physical pin numbers
	int rc = SetupWiringPiExtension();
	if (rc != 0)
		return rc;
	


	
	//  setup SPI (for MCP3008 ADC chip)
	rc = WiringPiSPISetup(SPI_CHAN, 500000);

	//  initialize MCP3008 chip
	rc = Mcp3008Setup(MCP3008_BASE, SPI_CHAN);
	
	//  Adafruit DC Motor Hat default address
	int fd = Pca9685Setup(PCA9685_BASE, 0x60, 100.0);
	if (fd != 0) 
	{
		// Reset all output 
		Pca9685PWMReset(fd); 
	}
	
	//  setup the keypad buttons
	PinMode(KeypadButton3Pin, PINMODE_INPUT);
	PinMode(LedPin, PINMODE_OUTPUT);
	DigitalWrite(LedPin, 1);
	usleep(2*SECONDMICRO);
	DigitalWrite(LedPin, 0);
	
	//  setup the PI pins for controlling motors
	PinMode(MAin1, PINMODE_PWM_OUTPUT);
	DigitalWrite(MAin1, 0);
	PinMode(MAin2, PINMODE_OUTPUT);
	DigitalWrite(MAin2, 0);
	
	CreateMotors();
	
	if (!TestButtons())
	{
		cout << "Test not complete." << endl; 
		getchar();
		return 1;
	}
	
	usleep(3*SECONDMICRO);
	
	if (!TestMotors())
	{
		cout << "Test not complete." << endl; 
		getchar();
		return 1;
	}
	
	if (!TestLights())
	{
		cout << "Test not complete." << endl; 
		getchar();
		return 1;
	}
	
	cout << "Test complete." << endl;
	getchar();
	return 0;
}
