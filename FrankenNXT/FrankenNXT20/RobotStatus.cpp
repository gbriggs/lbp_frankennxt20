#include "json.hpp"
#include "RobotStatus.h"


using namespace std;
using json = nlohmann::json;

//  Engine Room Status
EngineRoomStatus::EngineRoomStatus()
{
	Name = "Robot";	
}

EngineRoomStatus::EngineRoomStatus(string name)
{
	Name = name;
}

EngineRoomStatus::~EngineRoomStatus()
{
}

string EngineRoomStatus::SerializeAsJson()
{
	json j;
	j["Name"] = Name;
	j["EnginePower"] = EnginePower;
	j["EngineMotor1Power"] = EngineMotor1Power;
	j["EngineMotor2Power"] = EngineMotor2Power;
	j["EngineMotor1Rpm"] = EngineMotor1Rpm;
	j["EngineMotor2Rpm"] = EngineMotor2Rpm;
	j["Gear"] = Gear;
	j["Direction"] = Direction;
	j["FrontSteering"] = FrontSteering;
	j["RearSteering"] = RearSteering;
	
	return j.dump();
}



//  Button Status
ButtonStatus::ButtonStatus()
{
	Name = "Robot";	
}

ButtonStatus::ButtonStatus(string name)
{
	Name = name;
}

ButtonStatus::~ButtonStatus()
{
}

string ButtonStatus::SerializeAsJson()
{
	json j;
	j["Name"] = Name;
	j["LeftBtn"] = LeftBtn;
	j["EnterBtn"] = EnterBtn;
	j["RightBtn"] = RightBtn;
	j["DownBtn"] = DownBtn;
	
	return j.dump();
}



//  Controller Status
ControllerStatus::ControllerStatus()
{
	Name = "Robot";	
}

ControllerStatus::ControllerStatus(string name)
{
	Name = name;
}

ControllerStatus::~ControllerStatus()
{
}

string ControllerStatus::SerializeAsJson()
{
	json j;
	j["Name"] = Name;
	j["MainBattVoltage"] = MainBattVoltage;
	j["CpuTemp"] = CpuTemp;
	j["CpuUsage"] = CpuUsage;
	
	return j.dump();
}

	
	