#include <string>
#include <iostream>
#include <sstream>
#include <WiringPiExtensionApi.h>
#include "Logger.h"
#include "mainKeyboardInput.h"

using namespace std;



//  Prompt user with message and accept integer input from keyboard
bool ParseInteger(string text, int& number)
{
	string input;
	
	stringstream convertor;
		
	convertor << text;
	convertor >> number;

	if (convertor.fail())
		return false;
	else
		
		return true;
}	



//  Process the console input from the keyboard
bool ProcessKeyboardInput()
{
	string readLine;
	getline(cin, readLine);
		
	string key = "";
	if (readLine.length() > 0)
		key = readLine.substr(0, 1);
	
	if (key == "q")
	{
		return false;
	}
	else if (key == "l")
	{
		if (readLine.length() > 1)
		{
			string object = readLine.substr(1, 1);
			if ( readLine.length() > 2 && (object == "a" || object == "w"))
			{
				int logLevel;
				if (ParseInteger(readLine.substr(2), logLevel))
				{
					if (object == "a")
						Logging.ToggleAppLogLevel((LogLevel)logLevel);
					else
						Logging.ToggleWiringPiLogLevel((LogLevel)logLevel);
				}
				else
				{
					Logging.AddLog("mainKeyboardInput.cpp", "ProcessKeyboardInput", "Failed to parse log level.", LogLevelWarn);
				}
			}
			else
			{
				Logging.AddLog("mainKeyboardInput.cpp", "ProcessKeyboardInput", "Failed to parse log object.", LogLevelWarn);
			}
		}
		else
		{
			Logging.AddLog("mainKeyboardInput.cpp", "ProcessKeyboardInput", "Failed to parse log level.", LogLevelWarn);
		}
	}
	
	
	return true;
}
