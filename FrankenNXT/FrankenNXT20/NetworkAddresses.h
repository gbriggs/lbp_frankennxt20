#pragma once

//  UDP multicast for streaming data
#define MULTICAST_GROUPADDRESS ("234.5.6.7")
//  port for data and status
#define MULTICAST_STATUSPORT (48886)
//  port for logs
#define MULTICAST_LOGPORT (48885)


//  TCPIP Server for query and command
#define COMSERVER_PORT (47889)