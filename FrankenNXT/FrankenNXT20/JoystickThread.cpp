//  Joystick Thread
//  Code adapted from Jason White
//  https://gist.github.com/jasonwhite/c5b2048c15993d285130
//  
//  Supports XBox one first and second generation controllers
//  First generation with 11 buttons and second gen with 15 buttons
//  Button assignment is according to second generation
	

#include <fcntl.h>
#include <unistd.h>
#include <linux/joystick.h>
#include <string>
#include "JoystickThread.h"
#include "StringExtensions.h"
#include "Logger.h"

using namespace std;

//  Local functions declarations
int readEvent(int fd, struct js_event *event);
int getAxisCount(int fd);
int getButtonCount(int fd);
JoystickAxis setXBoxOneAxisState(struct js_event& event, struct axis_state axes[4]);
JoystickAxis setXBoxOneNewAxisState(struct js_event& event, struct axis_state axes[4]);
JoystickAxis setAxisState(struct js_event& event, struct axis_state axes[4]);
JoystickButton getXboxOneButtonNumber(struct js_event& event);
JoystickButton getButtonNumber(struct js_event& event);

//  joystick axes and buttons mapped according to second generation controller with 15 buttons
string axesNames[4] = { "LeftStick", "RightStick", "Triggers", "DPad" };
string buttonNames[15] = { "ABtn", "BBtn", "unknown", "XBtn", "YBtn", "unknown", "LeftBumper", "RightBumper", "unknown", "unknown", "unknown", "MenuBtn", "unknown", "LeftStick", "RightStick" };


//  Axis struct
struct axis_state {
	short x, y;
};

const double joystickAxisMax = 32767.0;

//  Properties of the joystick hardware are cached when we connect
int axisCount = 0;
int halfAxisCount = 0;
int buttonCount = 0;
string deviceName;
JoystickType joystickType;
//  Read timeout
fd_set fileDescriptorSet;
struct timeval timeout;



//  Constructor
//
JoystickThread::JoystickThread()
{
	Init();
}


//  Destructor
//
JoystickThread::~JoystickThread()
{
	if (ThreadRunning)
	{
		Cancel();
	}
	
	JoystickFileDescriptor = -1;
}


//  Initialize class
//
void JoystickThread::Init()
{
	JoystickFileDescriptor = -1;
	joystickType = jstUnknown;
	ConnectedDelegate = NULL;
	ButtonEventDelegate = NULL;
	AxisEventDelegate = NULL;
}


//  Override start function
//  open default joystick
void JoystickThread::Start()
{
	Start(string("/dev/input/js0"));
}
	

//  Start function
//  open specified joystick device
void JoystickThread::Start(string device)
{
	deviceName = device;
	Thread::Start();
}


//  Cancel function to stop the thread
//  override so we can close file descriptor before thread cancel
void JoystickThread::Cancel()
{
	if (JoystickFileDescriptor != -1)
	{
		close(JoystickFileDescriptor);
	}
	
	Thread::Cancel();
}


//  Connection Event
//
void JoystickThread::RegisterConnectedDelegate(JoystickConnectedDelegateFn delegate)
{
	ConnectedDelegate = delegate;
}
//
void JoystickThread::Connected(bool connected)
{
	if (ConnectedDelegate != NULL)
		ConnectedDelegate(deviceName, joystickType, connected);
}

	
//  Button Event
//
void JoystickThread::RegisterButtonEventDelegate(JoystickButtonEventDelegateFn delegate)
{
	ButtonEventDelegate = delegate;
}
//
void JoystickThread::ButtonEvent(JoystickButton button, bool state)
{
	if (ButtonEventDelegate != NULL)
		ButtonEventDelegate(button, state);
}


//  Axes Event
//
void JoystickThread::RegisterAxesEventDelegate(JoystickAxesEventDelegateFn delegate)
{
	AxisEventDelegate = delegate;
}
//
void JoystickThread::AxisEvent(JoystickAxis axis, double x, double y)
{
	if (AxisEventDelegate != NULL)
		AxisEventDelegate(axis, x, y);
}



//  Connect to joystick
//  will loop continuously until JS is connected
bool JoystickThread::ConnectToJoystick()
{
	bool connected = false;
	joystickType = jstUnknown;
	
	while (!connected && ThreadRunning)
	{
		JoystickFileDescriptor = open(deviceName.c_str(), O_RDONLY);

		if (JoystickFileDescriptor != -1)
		{
			//  setup the fd to read with a timeout
			FD_ZERO(&fileDescriptorSet); 
			FD_SET(JoystickFileDescriptor, &fileDescriptorSet); 
			timeout.tv_sec = 0;
			timeout.tv_usec = 10000;
			
			axisCount = getAxisCount(JoystickFileDescriptor);
			halfAxisCount = axisCount / 2;
			buttonCount = getButtonCount(JoystickFileDescriptor);
			
			switch (buttonCount)
			{
			case 15:
				joystickType = jstXboxOneNew;
				break;
			case 11:
				joystickType = jstXboxOne;
				break;
			default:
				Logging.AddLog("JoystickThread", "ConnectToJoystick", format("Connected to %s. %u Axes %u buttons", deviceName.c_str(), axisCount, buttonCount), LogLevelError);
				return false;
			}
				
			Logging.AddLog("JoystickThread", "ConnectToJoystick", format("Connected to %s. %u Axes %u buttons", deviceName.c_str(), axisCount, buttonCount), LogLevelDebug);
			Connected(true);
			return true;
		}
		
		Logging.AddLog("JoystickThread", "ConnectToJoystick", format("Attempting connection to %s", deviceName.c_str()), LogLevelTrace);
		Sleep(1000);
	}
	
	return false;
}


//  Joystick run function
//
void JoystickThread::RunFunction()
{
	struct js_event event;
	struct axis_state axes[4] = { 0 };
	JoystickAxis axis;
	
	ConnectToJoystick();
	
	while (ThreadRunning)
	{
		Sleep(1);
		int result = readEvent(JoystickFileDescriptor, &event);
		if (result == 0)
		{
			switch(event.type)
			{
			case JS_EVENT_BUTTON:
				{
					JoystickButton buttonNumber = getButtonNumber(event);
					if (buttonNumber > -1)
					{
						//Logging.AddLog("JoystickThread", "RunFunction", format("%s %s.", buttonNames[event.number].c_str(), event.value ? "pressed" : "released"), LogLevelTrace);
						ButtonEvent(buttonNumber, event.value == 1);
					}
				}
				break;
			case JS_EVENT_AXIS:
				{				
					axis = setAxisState(event, axes);
					if (axis != jsaUnknown)
					{
						//Logging.AddLog("JoystickThread", "RunFunction", format("%s at (%6d, %6d).", axesNames[axis].c_str(), axes[axis].x, axes[axis].y), LogLevelTrace);
						AxisEvent(axis, ((double)axes[axis].x / joystickAxisMax), ((double)axes[axis].y / joystickAxisMax));
					}
				}
				break;
			default:
				{
					/* Ignore init events. */
					Logging.AddLog("JoystickThread", "RunFunction", format("Event %u %u %u.", event.type, event.number, event.value), LogLevelDebug);
				}
				break;
			}
        
			fflush(stdout);
		}
		else if ( result > 0 )
		{
			Logging.AddLog("JoystickThread", "RunFunction", "Lost connection to the joystick.", LogLevelWarn);
			Connected(false);
			ConnectToJoystick();
		}
	}
}




//  Read joystick event from device
//  return 0 if success, or -1 for failure
int readEvent(int fd, struct js_event *event)
{
	ssize_t bytes;
	FD_SET(fd, &fileDescriptorSet); /* add our file descriptor to the set */
	auto rv = select(fd + 1, &fileDescriptorSet, NULL, NULL, &timeout);
	if (rv == -1)
	{
		Logging.AddLog("JoystickThread", "readEvent", "select error occured", LogLevelError);
	}
	else if (rv == 0)
	{	  
		return -1;	//  timeout
	}
	else
	{
		bytes = read(fd, event, sizeof(*event));
		if (bytes == sizeof(*event))
			return 0; //  success
	}
	
	return 1;	//  failure
}


//  Get the number of axes of the controller
//
int getAxisCount(int fd)
{
	__u8 axes;

	if (ioctl(fd, JSIOCGAXES, &axes) == -1)
		return 0;

	return axes;
}


//  Get the number of bottons of the controller
//
int getButtonCount(int fd)
{
	__u8 buttons;
	if (ioctl(fd, JSIOCGBUTTONS, &buttons) == -1)
		return 0;

	return buttons;
}


//  Parse axis state for XBox one controller
//
JoystickAxis setXBoxOneAxisState(struct js_event& event, struct axis_state axes[4])
{
	switch (event.number)
	{
	case 0:
		axes[0].x = event.value;
		return jsaLeftStick;
	case 1:
		axes[1].y = event.value;
		return jsaLeftStick;
	case 2:
		axes[2].y = event.value;
		return jsaTriggers;
	case 3:
		axes[1].x = event.value;
		return jsaRightStick;
	case 4:
		axes[1].y = event.value;
		return jsaRightStick;
	case 5:
		axes[2].x = event.value;
		return jsaTriggers;
	case 6:
		axes[3].x = event.value;
		return jsaDpad;
	case 7:
		axes[3].y = event.value;
		return jsaDpad;
	default:
		return jsaUnknown;
	}
}


//  Parse axis state for new XBox controller
//
JoystickAxis setNewXBoxOneAxisState(struct js_event& event, struct axis_state axes[4])
{
	int axis = event.number / 2;
	if (axis < halfAxisCount)
	{
		if (event.number % 2 == 0)
			axes[axis].x = event.value;
		else
			axes[axis].y = event.value;
	}
	return (JoystickAxis)axis;
}


//  Parse the axis state
//
JoystickAxis setAxisState(struct js_event& event, struct axis_state axes[4])
{
	switch (joystickType)
	{
	case jstXboxOne:
		return setXBoxOneAxisState(event, axes);
	case jstXboxOneNew:
		return setNewXBoxOneAxisState(event, axes);
	default:
		return jsaUnknown;
	}
}


//  Get button number for XBox one controller type
//
JoystickButton getXboxOneButtonNumber(struct js_event& event)
{
	switch (event.number)
	{
	case 2:
		return jsbX;	//  X button
	case 3 :
		return jsbY; 	//  Y button
	case 0 :
		return jsbA; 	//  A button
	case 1 :
		return jsbB; 	//  B button
	case 4 :
		return jsbLeftBumper; 	//  Left bumper
	case 5 :
		return jsbRightBumper; 	//  Right bumper
	case 6 :
		return jsbUnknown; 	//  squares button, not mapped to new ?
	case 7 :
		return jsbMenu; 	//  Menu button
	case 8 :
		return jsbHome; 	//  home button, not mapped to new ?
	case 9 :
		return jsbLeftStick;
	case 10:
		return jsbRightStick;
	default :
		return jsbUnknown;
	}
}


//  Get button number 
//  Button numbers are mapped to new xbox one controller type, so they just return the number
//  Old XBox one controller does translation to the new number
JoystickButton getButtonNumber(struct js_event& event)
{
	switch (joystickType)
	{
	case jstXboxOne:
		return getXboxOneButtonNumber(event);
	case jstXboxOneNew:
		return (JoystickButton)event.number;
	default:
		return jsbUnknown;
	}
}