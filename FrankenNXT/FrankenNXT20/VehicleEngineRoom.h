#pragma once
#include "Thread.h"

//  The Engine Room
//  Sets the state of the motors for speed, gear, and steering
//


#define DEGOFCIRCLE(x) (x/360.0)

class VehicleEngineRoom : public Thread
{
public:
	VehicleEngineRoom();
	virtual ~VehicleEngineRoom();
	
	//  Startup initialization function
	virtual void Start();
	
	//  Shut down clean up function
	void ShutDown();
	
	virtual void RunFunction();
	
	//  Engine room status
	double GetEnginePower();
	
	double GetEngineMotor1Power();
	
	double GetEngineMotor2Power();
	
	double GetEngineMotor1Rmp();
	
	double GetEngineMotor2Rpm();
	
	int GetTransmissionGear();
	
	double GetFrontSteering();
	
	double GetRearSteering();
	
	//  Set the power of both motors (-1.0 to 1.0)
	void SetEnginePower(double power);
	
	//  Set the power of motor1 or motor2 (-1.0 to 1.0)
	void SetMotorPower(int motor, double power);
	
	//  Set front steering position (-range to range)
	void SetFrontSteering(double circle);
	
	//  Set rear steering position (-range to range)
	void SetRearSteering(double circle);
	
	//  Set Transmission gear 0 = neutral, 1 = Low, 2 = High
	void SetTransmission(int gear);
	
	//  Sway steering for articulated vehicle
	void SetSwaySteering(double circleMin, double circleMax, double power);
	void SetSwaySteering(bool enable) { SwaySteerFront = enable; SwaySteerRear = enable;}
	bool GetSwaySteeringEnabled()	{ return SwaySteerFront == true || SwaySteerRear == true;}
	
	//
	void SetSwaySteeringFront(double circleMin, double circleMax, double power);
	void SetSwaySteeringFront(bool enable) { SwaySteerFront = enable;  }
	bool GetSwaySteeringFrontEnabled() {return SwaySteerFront == true;}
	
	//
	void SetSwaySteeringRear(double circleMin, double circleMax, double power);
	void SetSwaySteeringRear(bool enable) { SwaySteerRear = enable; }
	bool GetSwaySteeringRearEnabled() { return SwaySteerRear == true;}
	
	//  Stop all motors
	void FullStop();
	
	//  Reset counter of the steering encoders
	void ResetSteeringEncoders();
	
	//  Reset count of the transmission encoder
	void ResetTransmissionEncoder();
	
	//  Callback for motor encoder update
	void MotorEncoderUpdate(int motor, int tick);
	
protected:
	
	void CreateMotors();
	
	
	int MotorIndexEngineOne;
	int MotorIndexEngineTwo;
	int MotorIndexTransmission;
	int MotorIndexSteeringForward;
	int MotorIndexSteeringBack;

	double EnginePowerSetMotor1;
	double EnginePowerSetMotor2;
	
	bool SwaySteerFront;
	int SwaySteerFrontLastTick;
	double SwaySteerFrontPower;
	double SwaySteerFrontMinCircle;
	double SwaySteerFrontMaxCircle;
	
	bool SwaySteerRear;
	int SwaySteerRearLastTick;
	double SwaySteerRearPower;
	double SwaySteerRearMinCircle;
	double SwaySteerRearMaxCircle;
	
};
