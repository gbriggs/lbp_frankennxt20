#pragma once
#include <string>
#include <functional>
#include "Thread.h"
#include "BrickButtonDataTypes.h"


//  Brick Button Events
typedef std::function<void(BrickButton, bool)> BrickButtonEventDelegateFn;

//  Brick buttons monitor thread
//
class BrickButtonsThread : public Thread
{
public:
	BrickButtonsThread();
	virtual ~BrickButtonsThread();
	
	void RegisterButtonDelegate(BrickButtonEventDelegateFn delegate);
	void BrickButtonEvent(BrickButton button, bool state);
	
	virtual void RunFunction();
	
protected:
	
	void SetButtonState(BrickButton button, bool buttonDown);
	
	BrickButtonEventDelegateFn ButtonEventDelegate;
};