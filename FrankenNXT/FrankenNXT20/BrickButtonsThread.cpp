#include <netinet/in.h>
#include <string>
#include <algorithm>
#include "Logger.h"
#include "BrickButtonsThread.h"
#include "GpioSetup.h"
#include "StringExtensions.h"

using namespace std;

//  Local function declarations
int getAdcSwitchValue(int adcValue);

//  Keep track of button state
bool keypadButtonDown[4] = { false };
string keypadButtonNames[4] = { "1 (bottom)", "2 (right)", "3 (center)", "4 (left)" };


//  Constructor
//
BrickButtonsThread::BrickButtonsThread()
{
	ButtonEventDelegate = NULL;
}


//  Destructor
//
BrickButtonsThread::~BrickButtonsThread()
{
}


//  Button Event
//
void BrickButtonsThread::RegisterButtonDelegate(BrickButtonEventDelegateFn delegate)
{
	ButtonEventDelegate = delegate;
}
//
void BrickButtonsThread::BrickButtonEvent(BrickButton button, bool state)
{
	if (ButtonEventDelegate != NULL)
		ButtonEventDelegate(button, state);
}



// Set the button state, and update the bridge that button pushed
//
void BrickButtonsThread::SetButtonState(BrickButton button, bool buttonDown)
{
	if (keypadButtonDown[button] != buttonDown)
	{
		keypadButtonDown[button] = buttonDown;
		BrickButtonEvent(button, buttonDown);
		Logging.AddLog("ButtonsThread", "SetButtonState", format("Button %s %s.", keypadButtonNames[button].c_str(), buttonDown ? "down" : "up"), LogLevelDebug);
	}
}


//  Thread run function
//  waits on accept client TCPIP request, then handles the request
void BrickButtonsThread::RunFunction()
{
	Logging.AddLog("ButtonsThread", "RunFunction", "Starting ButtonsThread::RunFunction", LogLevelDebug);
	
	while (ThreadRunning)
	{
		//  get the digital button state
		auto readPin = DigitalRead(PIN_KEYPADBUTTON);
		SetButtonState(bkbEnter, readPin == 1);
		
		//  get adc button state
		//  will return number of button pushed (1,2,or 4) or 0 if no button pushed
		int adcSwitch = getAdcSwitchValue(AnalogRead(MCP3008_BASE + 1));
		for (int i = 1; i <= 4; i++)
		{
			if (i == 3)
				continue;	//  skip the digital button
			
			SetButtonState((BrickButton)(i - 1), adcSwitch == i);
		}
		
		Sleep(100);
	}
}



//  Get switch number from adc value
//
int getAdcSwitchValue(int adcValue)
{
	
	if (adcValue > 1000)
		return 1;
	else if (adcValue > 375)
		return 2;
	else if (adcValue > 100)
		return 4;
	else
		return 0;
}
