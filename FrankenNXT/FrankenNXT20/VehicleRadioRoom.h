#pragma once
#include "CommunicationThread.h"
#include "StatusMulticastThread.h"


//  Vehicle RadioRoom
//  Manages communication with external sources

class VehicleRadioRoom
{
public:
	
	VehicleRadioRoom();
	virtual ~VehicleRadioRoom();
	
	void RegisterGetKeyboardInputResponseDelegate(GetKeyboardInputResponseDelegateFn delegate);
	void RegisterGetEngineRoomStatusDelegate(GetEngineRoomStatusDelegateFn delegate);
	void RegisterGetControllerStatusDelegate(GetControllerStatusDelegateFn delegate);
	
	//  Startup initialization function
	void Start();
	
	//  Shut down clean up function
	void ShutDown();
	
protected:
	
	//  TCP Socket thread to accept external communications
	CommunicationThread CommsChannel;
	//
	GetKeyboardInputResponseDelegateFn GetKeyboardInputResponseDelegate;
	//
	std::string GetKeyboardInputResponse(int value);
	
	//  UDP multicast thread to broadcast status
	StatusMulticastThread StatusBroadcast;
	//
	GetEngineRoomStatusDelegateFn GetEngineRoomStatusDelegate;
	EngineRoomStatus GetEngineRoomStatus();
	//
	GetControllerStatusDelegateFn GetControllerStatusDelegate;
	ControllerStatus GetControllerStatus();
};
