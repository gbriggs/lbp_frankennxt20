#include <algorithm>
#include <cmath>
#include "HardwareMonitorThread.h"
#include "GpioSetup.h"
#include "WiringPiExtensionApi.h"
#include "Logger.h"
#include "StringExtensions.h"
#include "CollectionExtensions.h"
#include "CMD.h"

using namespace std;

//  Local function declarations
double getMainBatteryVoltage();
double getCpuTemperature();
double getCpuUsage();


//  Battery Voltage Constants
//  hardware: voltage battery +ve to ground minus voltage battery board +ve to ground
double voltageDropBattery = 0.0;
//  hardware:  voltage measured between ADC reference and ADC ground
double referenceVoltage = 3.3;
//  hardware:  voltage measured between brick (ADC) ground and battery board ground
double voltageDropBrick = 0.113;
//  hardware:  ADC voltage bias - average (adc voltage reading - true reading)
double adcVoltageBias = 0.0;
//  hardware:  voltage divider ratio with R1=5.6KOhm and R2 = 1.8 KOhm
double voltageDivider = (7.4/1.8);

//  CPU usage cached
static unsigned int lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle;


//  Hardware monitor Thread
//
HardwareMonitorThread::HardwareMonitorThread()
{
	MeanCpuTemp = -1.0;
	MeanCpuUsage = -1.0;
	MeanBatteryVoltage = -1.0;
}


//  Destructor
//
HardwareMonitorThread::~HardwareMonitorThread()
{
	
}


//  Get current CPU temp and update history
//
void HardwareMonitorThread::UpdateCpuTempHistory()
{		
	CpuTempHistory.push_front(getCpuTemperature());
	
	while (CpuTempHistory.size() > 5)
		CpuTempHistory.pop_back();
	
	MeanCpuTemp = DoubleMean(CpuTempHistory);
}


//  Get current CPU usage and update history
//
void HardwareMonitorThread::UpdateCpuUsageHistory()
{
	double cpu = getCpuUsage();
	if (std::isnan(cpu) || cpu < 0.0)
		return;
	
	CpuUsageHistory.push_front(cpu);
	
	while (CpuUsageHistory.size() > 5)
		CpuUsageHistory.pop_back();
	
	Logging.AddLog("PiMonitor", "UpdateCpuUsageHistory", format("Cpu usage: value %.3lf average %.3lf",cpu, DoubleMean(CpuUsageHistory)), LogLevelTrace);
	
	MeanCpuUsage = DoubleMean(CpuUsageHistory);
}


//  Get current battery voltage and update history
//
void HardwareMonitorThread::UpdateBatteryVoltageHistory()
{
	BatteryVoltageHistory.push_front(getMainBatteryVoltage());
			
	while (BatteryVoltageHistory.size() > 30)
		BatteryVoltageHistory.pop_back();
	
	MeanBatteryVoltage = DoubleMean(BatteryVoltageHistory);
}


//  Run function check timeouts
const int CheckCpuUsageTimeout = (1000);
const int CheckCpuTempTimeout = (1000);
const int CheckBatteryVoltageTimeout = (1000);


//  Run Function
//
void HardwareMonitorThread::RunFunction()
{
	unsigned int lastCpuUsageCheck = Millis();
	unsigned int lastCpuTempCheck = Millis();
	unsigned int lastBatteryVoltageCheck = Millis();
	
	while (ThreadRunning)
	{
		if (Millis() - lastCpuTempCheck >= CheckCpuTempTimeout)
		{
			UpdateCpuTempHistory();
			lastCpuTempCheck = Millis();
		}
		
		if (Millis() - lastCpuUsageCheck >= CheckCpuUsageTimeout)
		{			
			UpdateCpuUsageHistory();
			lastCpuUsageCheck = Millis();
		}
		
		if (Millis() - lastBatteryVoltageCheck >= CheckBatteryVoltageTimeout)
		{
			UpdateBatteryVoltageHistory();
			lastBatteryVoltageCheck = Millis();
		}
		
		Sleep(250);
	}
}


// Get battery voltage by reading ADC
//
double getMainBatteryVoltage()
{
	int value = AnalogRead(MCP3008_PINBATVOLTAGE);
	
	//	double adcVolts = ReferenceVoltage*((double)value / 1024.0) - AdcVoltageBias;
	//	double voltDivVolts = ReferenceVoltage*((double)value / 1024.0) - AdcVoltageBias + VoltageDropBrick;
	//double volts = (ReferenceVoltage*((double)value / 1024.0) - AdcVoltageBias + VoltageDropBrick) / (VoltageDividerRatio) + VoltageDropBattery;
	double adcVoltage = referenceVoltage*((double)value / 1024.0);
	double adcVoltageMultiplied = adcVoltage * voltageDivider;
	double volts = (adcVoltage) * (voltageDivider) + voltageDropBrick;
	Logging.AddLog("PiMonitor.cpp", "GetMainBatteryVoltage", format("Value %d  ADCV %3lf ADCx %3lf Volts %3lf", value, adcVoltage, adcVoltageMultiplied, volts), LogLevelTrace);
	
	return volts;
}


//  Get CPU temperature
//
double getCpuTemperature()
{
	float systemp, millideg;
	FILE *thermal;
	int n;

	thermal = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
	n = fscanf(thermal, "%f", &millideg);
	fclose(thermal);
	systemp = millideg / 1000;

	return (double)systemp;
}


//  Get CPU Usage percentage (reference https://www.raspberrypi.org/forums/viewtopic.php?t=229992#p1438922)
//
double getCpuUsage()
{
	float percent;
	int intPercent;
	std::string s;
     
	FILE* file;
	unsigned long long totalUser, totalUserLow, totalSys, totalIdle, total;
     
	file = fopen("/proc/stat", "r");
	fscanf(file, "cpu %llu %llu %llu %llu", &totalUser, &totalUserLow, &totalSys, &totalIdle);
	fclose(file);
     
	if (totalUser < lastTotalUser || totalUserLow < lastTotalUserLow ||
	    totalSys < lastTotalSys || totalIdle < lastTotalIdle)
	{
		//Overflow detection. Just skip this value.
		percent = -1.0;
	}
	else 
	{
		total = (totalUser - lastTotalUser) + (totalUserLow - lastTotalUserLow) + (totalSys - lastTotalSys);
		percent = total * 100;
		total += (totalIdle - lastTotalIdle);
		percent /= total;
	}
     
	lastTotalUser    = totalUser;
	lastTotalUserLow = totalUserLow;
	lastTotalSys     = totalSys;
	lastTotalIdle    = totalIdle;
     
	return percent;
}

