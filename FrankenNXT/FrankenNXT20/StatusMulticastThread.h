#pragma once
#include <functional>
#include "UdpMulticastServerThread.h"

class EngineRoomStatus;
class ControllerStatus;

typedef std::function<EngineRoomStatus(void)> GetEngineRoomStatusDelegateFn;
typedef std::function<ControllerStatus(void)> GetControllerStatusDelegateFn;


//  UDP multicast thread for status broadcast
//
class StatusMulticastThread : public UdpMulticastServerThread
{
public:
	StatusMulticastThread();
	virtual ~StatusMulticastThread();
	
	void RegisterGetEngineRoomStatusDelegate(GetEngineRoomStatusDelegateFn delegate);
	void RegisterGetControllerStatusDelegate(GetControllerStatusDelegateFn delegate);
	
	virtual void Start();
	
	virtual void RunFunction();
	
protected:
	
	GetEngineRoomStatusDelegateFn GetEngineRoomStatusDelegate;
	std::string GetEngineRoomStatus();
	GetControllerStatusDelegateFn GetControllerStatusDelegate;
	std::string GetControllerStatus();
	
	void BroadcastEngineRoomStatus();
	void BroadcastIpStatus();
	void BroadcastButtonStatus();
	void BroadcastControllerStatus();
};