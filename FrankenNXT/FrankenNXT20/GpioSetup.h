#pragma once
#include <string>

#define SPI_CHAN (0)
#define MCP3008_BASE (100)
#define MCP3008_PINBATVOLTAGE (MCP3008_BASE+6)
#define MCP3008_PINBUTTONS (MCP3008_BASE+0)

#define PCA9685_BASE (200)

#define PIN_KEYPADBUTTON (16)
#define PIN_LED (35)


//  Motor HBridge pins
#define PIN_MOTORAINPUT1 (22);
#define PIN_MOTORAINPUT2 (18);
//  Motor encoder input pins
#define PIN_MOTOR1ENCODERA (31)
#define PIN_MOTOR1ENCODERB (29)
#define PIN_MOTOR2ENCODERA (37)
#define PIN_MOTOR2ENCODERB (33)
#define PIN_MOTOR3ENCODERA (38)
#define PIN_MOTOR3ENCODERB (36)
#define PIN_MOTOR4ENCODERA (32)
#define PIN_MOTOR4ENCODERB (12)
#define PIN_MOTORAENCODERA (11)
#define PIN_MOTORAENCODERB (7)
#define PIN_MOTORBENCODERA (15)
#define PIN_MOTORBENCODERB (13)


//  Setup the GPIO pins on the Raspberry Pi for the FrankenNXT 2.0 Robot
//
bool SetupGpio();
