#pragma once

#include <string>
#include <condition_variable>
#include <queue>
#include <WiringPiExtensionApi.h>

#include "UdpMulticastServerThread.h"
#include "TerminalDisplay.h"


//  Logger Log
//  Container class for log info
class LoggerLog
{
public:
	
	LoggerLog(std::string sender, std::string function, std::string data, LogLevel level);
	
	LoggerLog(wpeLogEvent log);
	
	virtual ~LoggerLog();
	
	std::string SerializeAsJson();

	
	timeval Time;
	LogLevel Level;
	int Thread;
	std::string Sender;
	std::string Function;
	std::string Data;
	
};


//  Logger class
//  Takes care of writing logs and program output to the terminal
class Logger : public UdpMulticastServerThread
{
public:
	Logger();
	virtual ~Logger();
	
	void AddLog(wpeLogEvent log);
	
	void AddLog(std::string sender, std::string function, std::string data, LogLevel level);
	
	void WriteLineToDisplay(std::string log);

	void WriteLineToDisplay(const char* log);
	
	
	void PauseDisplayOutput();
	void ResumeDisplayOutput();
	bool IsDisplayOutputEnabled();
	
	void ToggleAppLogLevel(LogLevel level);
	void ToggleWiringPiLogLevel(LogLevel level);
	
	void EnableRemoteLogging(bool enable) { RemoteLoggingEnabled = enable; }

	
	//  thread overrides
	
	virtual void Start();
	
	virtual void Cancel();
	
	virtual void RunFunction();
	
protected:
	
	bool RemoteLoggingEnabled;
	
	//  queue lock
	std::mutex QueueMutex;
	std::queue<LoggerLog*> CommandQueue;
	
	//  queue notification
	bool Notified;
	std::condition_variable NotifyQueueCondition;
	std::mutex NotifyMutex;

	void Notify();
	
	//  display settings
	TerminalDisplay Display;
	bool DisplayOutputEnabled;
	LogLevel LogDisplayLevel;
	LogLevel LogLastLevelDisplayed;
	LogLevel LogLevelWiringPi;
	
	std::string LogLevelString(LogLevel level);
	int FgColorForLog(LogLevel level);
	int BgColorForLog(LogLevel level);
	
	
};


//  Declare Logging object for the application
extern Logger Logging;