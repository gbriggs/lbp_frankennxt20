#pragma once



//  Joystick Types Supported
enum JoystickType
{
	jstUnknown,
	jstXboxOne,
	jstXboxOneNew,
}
;

//  Joystick Buttons
enum JoystickButton
{
	jsbUnknown = -1,
	jsbA,
	jsbB,
	jsbUnkn2,
	jsbX,
	jsbY,
	jsbUnkn5,
	jsbLeftBumper,
	jsbRightBumper,
	jsbHome,
	jsbUnkn9,
	jsbUnkn10,
	jsbMenu,
	jsbUnkn12,
	jsbLeftStick,
	jsbRightStick
}
;

//  Joystickk Axes
enum JoystickAxis
{
	jsaUnknown = -1,
	jsaLeftStick,
	jsaRightStick,
	jsaTriggers, 	//  right on X, left on Y
	jsaDpad,
}
;
