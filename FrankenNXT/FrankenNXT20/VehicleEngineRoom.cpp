#include <math.h>
#include <WiringPiExtensionApi.h>
#include "VehicleEngineRoom.h"
#include "Logger.h"
#include "StringExtensions.h"
#include "GpioSetup.h"


#define GEARCIRCLETOLERANCE (10.0 / 360.0)


//  Reference to this class for static callbacks
VehicleEngineRoom* _this;


//   M1 on motor hat - PWM 08 / Input 10, 09
int M1in1 = PCA9685_BASE + 10;
int M1in2 = PCA9685_BASE + 9;
int M1pwm = PCA9685_BASE + 8;
//   M2 on motor hat - PWM 13 / Input 11, 12
int M2in1 = PCA9685_BASE + 11;
int M2in2 = PCA9685_BASE + 12;
int M2pwm = PCA9685_BASE + 13;
//   M3 on motor hat - PWM 02 / Input 04, 03
int M3in1 = PCA9685_BASE + 4;
int M3in2 = PCA9685_BASE + 3;
int M3pwm = PCA9685_BASE + 2;
//   M4 on motor hat - PWM 207 / Input 205, 206
int M4in1 = PCA9685_BASE + 5;
int M4in2 = PCA9685_BASE + 6;
int M4pwm = PCA9685_BASE + 7;

//  Extra Motor Controller
//  channel A
int MAin1 = PIN_MOTORAINPUT1;
int MAin2 = PIN_MOTORAINPUT2;
int MApwm = PCA9685_BASE + 0;
//  channel B
int MBin1 = PCA9685_BASE + 1;
int MBin2 = PCA9685_BASE + 14;
int MBpwm = PCA9685_BASE + 15;

//  Six Motor Connections
int Motor1;
int Motor2;
int Motor3;
int Motor4;
int MotorA;
int MotorB;

//  FrankenNXT Motor Encoder Wiring
int Motor1EncoderA = PIN_MOTOR1ENCODERA;
int Motor1EncoderB = PIN_MOTOR1ENCODERB;
int Motor2EncoderA = PIN_MOTOR2ENCODERA;
int Motor2EncoderB = PIN_MOTOR2ENCODERB;
int Motor3EncoderA = PIN_MOTOR3ENCODERA;
int Motor3EncoderB = PIN_MOTOR3ENCODERB;
int Motor4EncoderA = PIN_MOTOR4ENCODERA;
int Motor4EncoderB = PIN_MOTOR4ENCODERB;
int MotorAEncoderA = PIN_MOTORAENCODERA;
int MotorAEncoderB = PIN_MOTORAENCODERB;
int MotorBEncoderA = PIN_MOTORBENCODERA;
int MotorBEncoderB = PIN_MOTORBENCODERB;


//  Vehicle Engine Room
//
VehicleEngineRoom::VehicleEngineRoom() 
{
	_this = this;
	SwaySteerFront = false;
	SwaySteerRear = false;

}


// Destructor
//
VehicleEngineRoom::~VehicleEngineRoom()
{
	if (ThreadRunning)
	{
		Cancel();
	}
}


//  Create the motors in WiringPiExtension and 
//  start the engine room station thread
void VehicleEngineRoom::Start()
{
	CreateMotors();
	
	EnginePowerSetMotor1 = 0.0;
	EnginePowerSetMotor2 = 0.0;
	
	MotorWithRotaryEncoderStop(Motor1);
	MotorWithRotaryEncoderStop(Motor2);
	MotorWithRotaryEncoderStop(Motor3);
	MotorWithRotaryEncoderStop(Motor4);
	MotorWithRotaryEncoderStop(MotorA);
	MotorWithRotaryEncoderStop(MotorB);
	
	Thread::Start();
}


//  Shut down the engine room thread
//
void VehicleEngineRoom::ShutDown()
{
	Cancel();
}


//  Get Status
//
double VehicleEngineRoom::GetEnginePower()
{
	return (EnginePowerSetMotor1 + EnginePowerSetMotor2)/2.0;
}


double VehicleEngineRoom::GetEngineMotor1Power()
{
	return EnginePowerSetMotor1;
}


double VehicleEngineRoom::GetEngineMotor2Power()
{
	return EnginePowerSetMotor2;
}


double VehicleEngineRoom::GetEngineMotor1Rmp()
{
	return MotorWithRotaryEncoderGetRpm(MotorIndexEngineOne);
}


double VehicleEngineRoom::GetEngineMotor2Rpm()
{
	return MotorWithRotaryEncoderGetRpm(MotorIndexEngineTwo);
}



int VehicleEngineRoom::GetTransmissionGear()
{
	double circle = MotorWithRotaryEncoderGetCircle(MotorIndexTransmission);
	double origCircle = circle;
	while (circle > 1.0)
		circle -= 1.0;
	while (circle < 0)
		circle += 1.0;
	
	int gear = -1;
	
	if(fabs(.25 - circle) < GEARCIRCLETOLERANCE)
		gear= 2; 	// high gear
	else if(fabs(.75 - circle) < GEARCIRCLETOLERANCE)
		gear= 1;  	// high gear
	else if(fabs(0 - circle) < GEARCIRCLETOLERANCE || fabs(.5 - circle) < GEARCIRCLETOLERANCE)
		gear = 0; 	// neutral
	
	return gear;
}


double VehicleEngineRoom::GetFrontSteering()
{
	return MotorWithRotaryEncoderGetCircle(MotorIndexSteeringForward);
}


double VehicleEngineRoom::GetRearSteering()
{
	return MotorWithRotaryEncoderGetCircle(MotorIndexSteeringBack);
}



//  Set Engine Room

void VehicleEngineRoom::SetEnginePower(double power)
{
	MotorWithRotaryEncoderRun(MotorIndexEngineOne, power);
	MotorWithRotaryEncoderRun(MotorIndexEngineTwo, power);

	EnginePowerSetMotor1 = power;
	EnginePowerSetMotor2 = power;
	
	Logging.AddLog("VehicleEngineRoom", "SetEnginePower", format("Set power: %.3lf.", power), LogLevelDebug);
}
	

void VehicleEngineRoom::SetMotorPower(int motor, double power)
{
	switch (motor)
	{
	case 1:
		MotorWithRotaryEncoderRun(MotorIndexEngineOne, power);
		EnginePowerSetMotor1 = power;
		break;
		
	case 2:
		MotorWithRotaryEncoderRun(MotorIndexEngineTwo, power);
		EnginePowerSetMotor2 = power;
		break;
	}

	
	Logging.AddLog("VehicleEngineRoom", "SetMotorPower", format("Set motor %d power: %.3lf.", motor, power), LogLevelDebug);
}
	

void VehicleEngineRoom::SetFrontSteering(double circle)
{
	MotorWithRotaryEncoderHoldAt(MotorIndexSteeringForward, circle, .1);
	
	Logging.AddLog("VehicleEngineRoom", "SetFrontSteering", format("Set front steering %.5lf.", circle), LogLevelTrace);
}
	

void VehicleEngineRoom::SetRearSteering(double circle)
{
	MotorWithRotaryEncoderHoldAt(MotorIndexSteeringBack, circle, .1);
	
	Logging.AddLog("VehicleEngineRoom", "SetRearSteering", format("Set rear steering %.5lf.", circle), LogLevelTrace);
	
}


void VehicleEngineRoom::ResetSteeringEncoders()
{
	MotorWithRotaryEncoderResetCount(MotorIndexSteeringForward, 0);
	MotorWithRotaryEncoderResetCount(MotorIndexSteeringBack, 0);	
	
	Logging.AddLog("VehicleEngineRoom", "ResetSteeringEncoders", "Reset encoders.", LogLevelInfo);
}


void VehicleEngineRoom::ResetTransmissionEncoder()
{
	MotorWithRotaryEncoderResetCount(MotorIndexTransmission, 0);
		
	Logging.AddLog("VehicleEngineRoom", "ResetTransmissionEncoder", "Reset encoder.", LogLevelInfo);
}


void VehicleEngineRoom::SetTransmission(int gear)
{
	switch (gear)
	{
	case -1:
		MotorWithRotaryEncoderRun(MotorIndexTransmission, .15);
		break;
	case 0:
		MotorWithRotaryEncoderHoldAt(MotorIndexTransmission, 0.0, .2);
		break;
		
	case 1:
		MotorWithRotaryEncoderHoldAt(MotorIndexTransmission, -.25, .2);
		break;
		
	case 2:
		MotorWithRotaryEncoderHoldAt(MotorIndexTransmission, .25, .2);
		break;
	default:
		Logging.AddLog("VehicleEngineRoom", "SetTransmission", format("Invalid gear %d",gear), LogLevelError);
		return;
	}
	
	Logging.AddLog("VehicleEngineRoom", "SetTransmission", format("Set gear %d", gear), LogLevelDebug);
}


void VehicleEngineRoom::FullStop()
{
	MotorWithRotaryEncoderStop(Motor1);
	MotorWithRotaryEncoderStop(Motor2);
	MotorWithRotaryEncoderStop(Motor3);
	MotorWithRotaryEncoderStop(Motor4);
	MotorWithRotaryEncoderStop(MotorA);
	MotorWithRotaryEncoderStop(MotorB);
	
	EnginePowerSetMotor1 = 0.0;
	EnginePowerSetMotor2 = 0.0;
	
	Logging.AddLog("VehicleEngineRoom", "FullStop", "Full stop.", LogLevelDebug);
}



void VehicleEngineRoom::SetSwaySteering(double circleMin, double circleMax, double power)
{
	SetSwaySteeringFront(circleMin, circleMax, power);
	SetSwaySteeringRear(circleMin, circleMax, power);
}	


void VehicleEngineRoom::SetSwaySteeringFront(double circleMin, double circleMax, double power)
{
	SwaySteerFront = true;
	SwaySteerFrontMinCircle = circleMin;
	SwaySteerFrontMaxCircle = circleMax;
	SwaySteerFrontPower = power;
	SwaySteerFrontLastTick = MotorWithRotaryEncoderGetTick(MotorIndexSteeringForward);
	if (SwaySteerFrontLastTick > 0)
		MotorWithRotaryEncoderHoldAt(MotorIndexSteeringForward, SwaySteerFrontMinCircle, SwaySteerFrontPower);
	else
		MotorWithRotaryEncoderHoldAt(MotorIndexSteeringForward, SwaySteerFrontMaxCircle, SwaySteerFrontPower);
}

void VehicleEngineRoom::SetSwaySteeringRear(double circleMin, double circleMax, double power)
{
	SwaySteerRear = true;
	SwaySteerRearMinCircle = circleMin;
	SwaySteerRearMaxCircle = circleMax;
	SwaySteerRearPower = power;
	SwaySteerRearLastTick = MotorWithRotaryEncoderGetTick(MotorIndexSteeringBack);
	if (SwaySteerRearLastTick > 0)
		MotorWithRotaryEncoderHoldAt(MotorIndexSteeringBack, SwaySteerRearMinCircle, SwaySteerRearPower);
	else
		MotorWithRotaryEncoderHoldAt(MotorIndexSteeringBack, SwaySteerRearMaxCircle, SwaySteerRearPower);
}


//  Engine Room Thread
void VehicleEngineRoom::RunFunction()
{
	while (ThreadRunning)
	{	
		Sleep(10);
	}
}

//  Motor Encoder Functions
//

void VehicleEngineRoom::MotorEncoderUpdate(int motor, int tick)
{
	if (motor == MotorIndexSteeringForward && SwaySteerFront)
	{
		double circle = MotorWithRotaryEncoderGetCircle(MotorIndexSteeringForward);
		if (circle <= (SwaySteerFrontMinCircle + fabs(SwaySteerFrontMinCircle)*.01) && (tick - SwaySteerFrontLastTick) < 0)
		{
			MotorWithRotaryEncoderHoldAt(MotorIndexSteeringForward, SwaySteerFrontMaxCircle, SwaySteerFrontPower);
		}
		else if (circle >= (SwaySteerFrontMaxCircle - fabs(SwaySteerFrontMaxCircle)*.01) && (tick - SwaySteerFrontLastTick) > 0)
		{
			MotorWithRotaryEncoderHoldAt(MotorIndexSteeringForward, SwaySteerFrontMinCircle, SwaySteerFrontPower);
		}
		SwaySteerFrontLastTick = tick;
	}
	else if (motor == MotorIndexSteeringBack && SwaySteerRear)
	{
		double circle = MotorWithRotaryEncoderGetCircle(MotorIndexSteeringBack);
		if (circle <= (SwaySteerRearMinCircle + fabs(SwaySteerRearMinCircle)*.01) && (tick - SwaySteerRearLastTick) < 0)
		{
			MotorWithRotaryEncoderHoldAt(MotorIndexSteeringBack, SwaySteerRearMaxCircle, SwaySteerRearPower);
		}
		else if (circle >= (SwaySteerRearMaxCircle - fabs(SwaySteerRearMaxCircle)*.01) && (tick - SwaySteerRearLastTick) > 0)
		{
			MotorWithRotaryEncoderHoldAt(MotorIndexSteeringBack, SwaySteerRearMinCircle, SwaySteerRearPower);
		}
		SwaySteerRearLastTick = tick;
	}
}

void Motor1EncoderCallback(int tick)
{
	_this->MotorEncoderUpdate(Motor1, tick);
}

void Motor2EncoderCallback(int tick)
{
	_this->MotorEncoderUpdate(Motor2, tick);
}

void Motor3EncoderCallback(int tick)
{
	_this->MotorEncoderUpdate(Motor3, tick);
}

void Motor4EncoderCallback(int tick)
{
	_this->MotorEncoderUpdate(Motor4, tick);
}

void MotorAEncoderCallback(int tick)
{
	_this->MotorEncoderUpdate(MotorA, tick);
}

void MotorBEncoderCallback(int tick)
{
	_this->MotorEncoderUpdate(MotorB, tick);
}


void VehicleEngineRoom::CreateMotors()
{
	//  Motor 1 
	//   HBridge MotorA
	Motor1 = MotorWithRotaryEncoderCreate(MAin1, MAin2, MApwm, Motor1EncoderA, Motor1EncoderB, -1, 180, Motor1EncoderCallback);
	MotorWithRotaryEncoderStop(Motor1);
	
	
	//  Motor 2 
	//   HBridge MotorB
	Motor2 = MotorWithRotaryEncoderCreate(MBin1, MBin2, MBpwm, Motor2EncoderA, Motor2EncoderB, -1, 180, Motor2EncoderCallback);
	MotorWithRotaryEncoderStop(Motor2);
	
	//  Motor 3 
	//   M1 on motor hat
	Motor3 = MotorWithRotaryEncoderCreate(M1in1, M1in2, M1pwm, Motor3EncoderA, Motor3EncoderB, -1, 180, Motor3EncoderCallback);
	MotorWithRotaryEncoderStop(Motor3);
	
	//  Motor 4 
	//   M2 on motor hat
	Motor4 = MotorWithRotaryEncoderCreate(M2in1, M2in2, M2pwm, Motor4EncoderA, Motor4EncoderB, -1, 180, Motor4EncoderCallback);
	MotorWithRotaryEncoderStop(Motor4);
	
	//  Motor A 
	//   M3 on motor hat
	MotorA = MotorWithRotaryEncoderCreate(M3in1, M3in2, M3pwm, MotorAEncoderA, MotorAEncoderB, -1, 180, MotorAEncoderCallback);
	MotorWithRotaryEncoderStop(MotorA);
	
	//  Motor B 
	//   M4 on motor hat
	MotorB = MotorWithRotaryEncoderCreate(M4in1, M4in2, M4pwm, MotorBEncoderA, MotorBEncoderB, -1, 180, MotorBEncoderCallback);
	MotorWithRotaryEncoderStop(MotorB);
	
	//  FrankeNXT 2.0 Setup
	MotorIndexEngineOne = Motor1;
	MotorIndexEngineTwo = Motor4;
	MotorIndexTransmission = Motor2;
	MotorIndexSteeringForward = MotorA;
	MotorIndexSteeringBack = Motor3;
}