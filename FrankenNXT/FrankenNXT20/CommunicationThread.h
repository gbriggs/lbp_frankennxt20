#pragma once
#include <string>
#include <functional>
#include "TCPServerThread.h"


typedef std::function<std::string(int)> GetKeyboardInputResponseDelegateFn;

//  TCP Server thread waits for GET/POST from remote client applicaiton
//
class CommunicationThread : public TCPServerThread
{
public:
	CommunicationThread();
	virtual ~CommunicationThread();
	
	void RegisterGetKeyboardInputResponseDelegate(GetKeyboardInputResponseDelegateFn delegate);
	
	virtual void Start();
	
	virtual void RunFunction();
	
protected:
	
	void HandleKeyboardInputRequest(int acceptFileDesc, std::string args);
	void HandleLogLevelChangeRequest(int acceptFileDesc, std::string args);
	
	GetKeyboardInputResponseDelegateFn GetKeyboardInputResponseDelegate;
	
	std::string GetKeyboardInputResponse(int value);
};