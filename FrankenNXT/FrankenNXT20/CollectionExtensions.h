#pragma once

#include <numeric>
#include <algorithm>
#include <list>
#include <vector>



struct IntSumStruct
{
	void operator()(int n) { sum += n; }
	int sum{0}
	;
};


struct IntMeanStruct
{
	void operator()(int n)
	{
		sum(n);
		count++;
	}
	
	IntSumStruct sum;
	
	int count{0}
	;
	
	double Average()
	{
		if (count == 0)
			return 0.0;
		return (sum.sum / (double)count);
	}
};



struct DoubleSumStruct
{
	void operator()(double n) { sum += n; }
	double sum{0};
};



struct DoubleMeanStruct
{
	void operator()(double n)
	{
		sum(n);
		count++;
	}
	
	DoubleSumStruct sum;
	
	int count{0}
	;
	
	double Mean()
	{
		if (count == 0)
			return 0.0;
		return (sum.sum / (double)count);
	}
};


//  Calculate the mean of a list of doubles
//
inline double DoubleMean(std::list<double> values)
{
	auto mean = for_each(values.begin(), values.end(), DoubleMeanStruct());
	return mean.Mean();
}


//  Calculate the deviation of a list of doubles
//
inline double DoubleDeviation(std::list<double> values)
{
	if (values.size() < 2)
		return 0.0;
	
	double mean = DoubleMean(values);
	std::vector<double> diff(values.size());
	
	transform(values.begin(), values.end(), diff.begin(), [mean](double x) { return x - mean; });
	double sq_sum = inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);

	return std::sqrt(sq_sum / values.size());
}