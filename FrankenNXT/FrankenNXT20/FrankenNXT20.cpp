#include <iostream>
#include <WiringPiExtensionApi.h>
#include "GpioSetup.h"
#include "VehicleBridge.h"
#include "VehicleEngineRoom.h"
#include "VehicleRadioRoom.h"
#include "BrickButtonsThread.h"
#include "JoystickThread.h"
#include "mainKeyboardInput.h"
#include "Logger.h"
#include "RobotStatus.h"


using namespace std;

//  Logging component
Logger Logging;

//  Logging callback for WiringPiExtension	
void OnLog(wpeLogEvent log)
{
	Logging.AddLog(log);
}

//  Vehicle Components
BrickButtonsThread BrickButtonMonitor;
JoystickThread Joystick;
VehicleEngineRoom TheEngineRoom;
VehicleRadioRoom TheRadioRoom;
VehicleBridge TheBridge(TheEngineRoom);

//  Startup
//
void Startup()
{
	TheBridge.Start();
	TheEngineRoom.Start();
	TheRadioRoom.Start();
	Joystick.Start();
	BrickButtonMonitor.Start();
}
	

//  Shut Down
//
void ShutDown()
{
	BrickButtonMonitor.Cancel();
	Joystick.Cancel();
	TheRadioRoom.ShutDown();
	TheEngineRoom.ShutDown();
	TheBridge.ShutDown();
}


//  Main Entry Point
//
int main(int argc, char *argv[])
{
	//  setup logging
	Logging.Start();
	SetLoggingCallback(OnLog);
	
	//  configure the GPIO pins used by the device
	if (!SetupGpio())
	{
		Logging.AddLog("FrankenNXT20.cpp", "main", "Failed to setup GPIO.", LogLevelFatal);
		ShutDown();
		return -1;
	}
	
	//  wire up the systems to the bridge
	//
	//  communications channel from the radio room
	TheRadioRoom.RegisterGetKeyboardInputResponseDelegate([](int value) {return TheBridge.OnKeyboardInputEvent(value);});
	TheRadioRoom.RegisterGetEngineRoomStatusDelegate([](void) {return TheBridge.GetEngineRoomStatus();});
	TheRadioRoom.RegisterGetControllerStatusDelegate([](void) {return TheBridge.GetControllerStatus();});
	//
	//  brick buttons monitor
	BrickButtonMonitor.RegisterButtonDelegate([](BrickButton button, bool state) {TheBridge.OnBrickButtonEvent(button, state); });
	//
	//  joystick
	Joystick.RegisterConnectedDelegate([](string device, JoystickType type, bool connected) {TheBridge.OnJoystickConnected(device, type, connected); });
	Joystick.RegisterButtonEventDelegate([](JoystickButton button, bool state) {TheBridge.OnJoystickButtonEvent(button, state); });
	Joystick.RegisterAxesEventDelegate([](JoystickAxis axis, double x, double y) {TheBridge.OnJoystickAxisEvent(axis, x, y); });
	
	//  start the components
	Startup();

	//  program started
	Logging.AddLog("FrankenNXT20.cpp", "main", "Program started", LogLevelInfo);
	
	//  wait for command line input
	bool keepRunning = true;
	while (keepRunning)
	{
		keepRunning = ProcessKeyboardInput();
	}
		
	ShutDown();
	Logging.Cancel();
	
	return 0;
}