#include "VehicleRadioRoom.h"
#include "RobotStatus.h"

using namespace std;

//  Constructor
//
VehicleRadioRoom::VehicleRadioRoom()
{
	GetKeyboardInputResponseDelegate = NULL;
	
	CommsChannel.RegisterGetKeyboardInputResponseDelegate([this](int value) {return this->GetKeyboardInputResponse(value);});
	StatusBroadcast.RegisterGetEngineRoomStatusDelegate([this](void) {return this->GetEngineRoomStatus();});
	StatusBroadcast.RegisterGetControllerStatusDelegate([this](void) {return this->GetControllerStatus();});
}

//  Destructor
//
VehicleRadioRoom::~VehicleRadioRoom()
{
}


// Keyboard input events
void VehicleRadioRoom::RegisterGetKeyboardInputResponseDelegate(GetKeyboardInputResponseDelegateFn delegate)
{
	GetKeyboardInputResponseDelegate = delegate;
}
//
string VehicleRadioRoom::GetKeyboardInputResponse(int value)
{
	if (GetKeyboardInputResponseDelegate != NULL)
		return GetKeyboardInputResponseDelegate(value);
	
	return "";
}


//  Register get engine room status delegate function
//
void VehicleRadioRoom::RegisterGetEngineRoomStatusDelegate(GetEngineRoomStatusDelegateFn delegate)
{
	GetEngineRoomStatusDelegate = delegate;
}
//
EngineRoomStatus VehicleRadioRoom::GetEngineRoomStatus()
{
	if (GetEngineRoomStatusDelegate != NULL)
		return GetEngineRoomStatusDelegate();
	
	EngineRoomStatus empty;
	return empty;
}


//  Register get engine room status delegate function
void VehicleRadioRoom::RegisterGetControllerStatusDelegate(GetControllerStatusDelegateFn delegate)
{
	GetControllerStatusDelegate = delegate;
}
//
ControllerStatus VehicleRadioRoom::GetControllerStatus()
{
	if (GetControllerStatusDelegate != NULL)
		return GetControllerStatusDelegate();
	
	ControllerStatus empty;
	return empty;
}


//  Start radio room substations
//
void VehicleRadioRoom::Start()
{
	CommsChannel.Start();
	StatusBroadcast.Start();
}


//  Shut down radio room sub stations
//
void VehicleRadioRoom::ShutDown()
{
	CommsChannel.Cancel();
	StatusBroadcast.Cancel();
}
