#pragma once
#include <list>
#include "Thread.h"


class HardwareMonitorThread : public Thread
{
public:

	HardwareMonitorThread();
	virtual ~HardwareMonitorThread();
	
	//  Properties
	double GetMeanCpuTemp() { return MeanCpuTemp; }
	double GetMeanCpuUsage() {return MeanCpuUsage; }
	double GetMeanBatteryVoltage() {return MeanBatteryVoltage;}
	
	virtual void RunFunction();
	
protected:
	
	std::list<double> CpuTempHistory;
	double MeanCpuTemp;
	void UpdateCpuTempHistory();
	
	std::list<double> CpuUsageHistory;
	double MeanCpuUsage;
	void UpdateCpuUsageHistory();
	
	std::list<double> BatteryVoltageHistory;
	double MeanBatteryVoltage;
	void UpdateBatteryVoltageHistory();
	
};

