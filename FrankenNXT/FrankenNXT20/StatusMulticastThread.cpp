#include "StatusMulticastThread.h"
#include "Logger.h"
#include "VehicleBridge.h"
#include "StringExtensions.h"
#include "CMDiwconfig.h"
#include "RobotStatus.h"
#include "TimeExtensions.h"
#include "NetworkAddresses.h"
#include "NetworkExtensions.h"

using namespace std;

//  Constructor
//
StatusMulticastThread::StatusMulticastThread()
{
	SocketFileDescriptor = -1;	
	GetEngineRoomStatusDelegate = NULL;
}


//  Destructor
//
StatusMulticastThread::~StatusMulticastThread()
{
}


//  Register get engine room status delegate function
void StatusMulticastThread::RegisterGetEngineRoomStatusDelegate(GetEngineRoomStatusDelegateFn delegate)
{
	GetEngineRoomStatusDelegate = delegate;
}
string StatusMulticastThread::GetEngineRoomStatus()
{
	if (GetEngineRoomStatusDelegate != NULL)
	{
		auto test = GetEngineRoomStatusDelegate();
		return GetEngineRoomStatusDelegate().SerializeAsJson();
	
	}
	return "";
}



//  Register get engine room status delegate function
void StatusMulticastThread::RegisterGetControllerStatusDelegate(GetControllerStatusDelegateFn delegate)
{
	GetControllerStatusDelegate = delegate;
}
string StatusMulticastThread::GetControllerStatus()
{
	if (GetControllerStatusDelegate != NULL)
	{
		auto test = GetControllerStatusDelegate();
		return GetControllerStatusDelegate().SerializeAsJson();
		
	}
	return "";
}


//  Start the thread
//  override to open the server socket for multicast before starting thread
void StatusMulticastThread::Start()
{
	int port = OpenServerSocket(MULTICAST_STATUSPORT, MULTICAST_GROUPADDRESS);
	
	if (port < 0)
	{
		Logging.AddLog("StatusMulticastThread", "Start", "Unable to open server socket port.", LogLevelFatal);
		return;
	}
	
	UdpMulticastServerThread::Start();
}

//  broadcast timeouts for different status properties
int BroadcastTimeoutEngineRoom = (100);
int BroadcastTimeoutIpinfo = (10 * 1000);
int  BroadcastTimeoutButtons = (200);
int BroadcastTimeoutController = (5 * 1000);


//  Run function
//
void StatusMulticastThread::RunFunction()
{
	unsigned int lastEngineRoomBroadcast = Millis();
	unsigned int lastIpBroadcast = Millis();
	unsigned int lastButtonBroadcast = Millis();
	unsigned int lastControllerBroadcast = Millis();
	
	while (ThreadRunning)
	{
		if (Millis() - lastEngineRoomBroadcast >= BroadcastTimeoutEngineRoom)
		{
			BroadcastEngineRoomStatus();
			lastEngineRoomBroadcast = Millis();
		}
		
		if (Millis() - lastIpBroadcast >= BroadcastTimeoutIpinfo)
		{
			BroadcastIpStatus();
			lastIpBroadcast = Millis();
		}
		
		if (Millis() - lastButtonBroadcast >= BroadcastTimeoutButtons)
		{
			BroadcastButtonStatus();
			lastButtonBroadcast = Millis();
		}
		
		if (Millis() - lastControllerBroadcast >= BroadcastTimeoutController)
		{
			BroadcastControllerStatus();
			lastControllerBroadcast = Millis();
		}
		
		Sleep(10);
	}
}


//  Broadcast the engine room status
//
void StatusMulticastThread::BroadcastEngineRoomStatus()
{
	string statusString = format("motorstatus?status=%s&time=%lld\n", GetEngineRoomStatus().c_str(), GetUnixTimeMilliseconds());
	WriteMulticastString(statusString);
}


string eth0Address = "";
string wlan0Address = "";

void setIpConfig()
{	
	auto addresses = GetNetworkIp4Addresses();
	for (auto it = addresses.begin(); it != addresses.end(); ++it)
	{
		if (get<0>(*it).compare("eth0") == 0)
		{
			eth0Address = get<1>(*it);
		}
		else if (get<0>(*it).compare("wlan0") == 0)
		{
			wlan0Address = get<1>(*it);
		}
	}
}


//  Broadcast the network connection status
//
void StatusMulticastThread::BroadcastIpStatus()
{
	CMDiwconfig iwconfig;
	setIpConfig();
	if (iwconfig.Execute())
	{
		string wiFiStatus;
		if (iwconfig.Mode() == iwcMaster)
			wiFiStatus = format("wlanmode=%s&wlanssid=%s", iwconfig.ModeString().c_str(), iwconfig.Essid().c_str());
		else
			wiFiStatus = format("wlanmode=%s", iwconfig.ModeString().c_str());
				
		string networkString = format("networkstatus?eth0=%s&wlan0=%s&%s&time=%lld\n", eth0Address.c_str(), wlan0Address.c_str(), wiFiStatus.c_str(), GetUnixTimeMilliseconds());
				
		WriteMulticastString(networkString);
	}
	else
	{
		Logging.AddLog("StatusMulticastThread", "RunFunction", "Failed to execute ifconfig or iwconfig.", LogLevelFatal);	
	}
}


//  Broadcast the button status
//
void StatusMulticastThread::BroadcastButtonStatus()
{
	//  TODO - broadcast if there is anything in the change queue
}


//  Broadcast the controller status
//
void StatusMulticastThread::BroadcastControllerStatus()
{	
	string statusString = format("controlstatus?status=%s&time=%lld\n", GetControllerStatus().c_str(), GetUnixTimeMilliseconds());
	WriteMulticastString(statusString);
}
