#pragma once
#include <string>

//  Robot Status Classes
//  Keep these in sync with the client side RobotStatus.cs classes

//  Base Class
class RobotStatus
{
public:
	std::string Name;
	
	virtual std::string SerializeAsJson() {return "";};
};


// Engine Room Status
class EngineRoomStatus : public RobotStatus
{
public:
	EngineRoomStatus();
	EngineRoomStatus(std::string name);
	virtual ~EngineRoomStatus();
	double EnginePower;
	double EngineMotor1Power;
	double EngineMotor2Power;
	double EngineMotor1Rpm;
	double EngineMotor2Rpm;
	int Gear;
	int Direction;
	double FrontSteering;
	double RearSteering;
	
	virtual std::string SerializeAsJson();
};


//  Button status
class ButtonStatus : public RobotStatus
{
public:
	ButtonStatus();
	ButtonStatus(std::string name);
	virtual ~ButtonStatus();
	
	bool LeftBtn;
	bool EnterBtn;
	bool RightBtn;
	bool DownBtn;
	
	virtual std::string SerializeAsJson();
}
;


//  Microcontroller Status
class ControllerStatus	: public RobotStatus
{
public:
	ControllerStatus();
	ControllerStatus(std::string name);
	virtual ~ControllerStatus();
	
	double MainBattVoltage;
	double CpuTemp;
	double CpuUsage;

	virtual std::string SerializeAsJson();
};





