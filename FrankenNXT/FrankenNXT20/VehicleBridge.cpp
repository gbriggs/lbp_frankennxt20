#include <math.h>
#include "VehicleBridge.h"
#include "VehicleEngineRoom.h"
#include "Logger.h"
#include "StringExtensions.h"
#include "JSonHelpers.h"
#include "KeyMap.h"
#include "RobotStatus.h"
#include "GpioSetup.h"


using namespace std;


//  Constructor
//
VehicleBridge::VehicleBridge(VehicleEngineRoom& engineRoom) :
	TheEngineRoom(engineRoom)
{
	RobotName = "FrankenNXT20";
	
	DrivingDirection = 1;	//  forward
	
	JoystickMode = FourWheelSteer;
	
}


//  Destructor
//
VehicleBridge::~VehicleBridge()
{
}
	

//  Start the bridge operations, and start all its substations
//
void VehicleBridge::Start()
{
	HardwareMonitor.Start();

}


//  Shut down all bridge sub stations and shut down bridge
//
void VehicleBridge::ShutDown()
{
	
	HardwareMonitor.Cancel();
}


//  Handle keyboard input request
//
string VehicleBridge::OnKeyboardInputEvent(int key)
{
	Logging.AddLog("VehicleBridge", "KeyboardInputRequest", format("Request keyboard input %d", key), LogLevelTrace);
	
	switch ((KeyboardKey)key)
	{
		//  Engine Power W/S E/D X
	case KeyW :	//  w = increase speed by 1
		{
			ChangeEnginePower(0.01);
		}
		break;
		
	case KeyS :	//  s = decrease speed by 1
		{
			ChangeEnginePower(-0.01);
		}
		break;
		
	case KeyE : //  e = increase speed by 10
		{
			ChangeEnginePower(0.10);
		}
		break;
		
	case KeyD : //  d = decrease speed by 10
		{
			ChangeEnginePower(-0.10);
		}
		break;
		
	case KeyX : //  x = stop engine motors
		{
			TheEngineRoom.SetEnginePower(0.0);
		}
		break;
		
		
		//  Steering R-T  F-G
	case KeyR : //  r = forward turn left
		{
			ChangeFrontSteering(DEGOFCIRCLE(4.0));
		}
		break;
		
	case KeyT : //  t = forward turn right
		{
			ChangeFrontSteering(-DEGOFCIRCLE(4.0));
		}
		break;
		
	case KeyF : //  f = rear turn left
		{
			ChangeRearSteering(DEGOFCIRCLE(4.0));
		}
		break;
		
	case KeyG :	//  g = rear turn right
		{
			ChangeRearSteering(-DEGOFCIRCLE(4.0));
		}
		break;
		
	
		//  Transmission V-B-N
	case KeyV :	//  v = transmission low gear
		{
			TheEngineRoom.SetTransmission(1);
		}
		break;
		
	case KeyB :	//  b = transmission neutral
		{
			TheEngineRoom.SetTransmission(0);
		}
		break;
		
	case KeyN : //  n = transmission high gear
		{
			TheEngineRoom.SetTransmission(2);
		}
		break;
		
			
		//  Misc Keys
		
		case KeyZ :  //  z = full stop
		{
			TheEngineRoom.FullStop();
		}
		break;
		
	case KeyQ :	//  q = reset steering rotary encoder count
		{
			TheEngineRoom.ResetSteeringEncoders();
		}
		break;
		
	case KeyA :	//  a = reset transmission encoder count
		{
			TheEngineRoom.ResetTransmissionEncoder();
		}
		break;
		
	case KeyY : //  Y = Sway Steering Demo
		{
			if (TheEngineRoom.GetSwaySteeringEnabled())
			{
				TheEngineRoom.SetSwaySteering(false);
			}
			else
			{
				TheEngineRoom.SetSwaySteering(-.20, .20, .08);
			}
		}
		break;
		
	default:
		return format("NAK?response='%d' is not a valid key.\n", key);
	}
	
	
	return format("ACK?response=Key %d accepted.\n", key);
}


//  Joystick
double JoystickSetSteeringLastFrontCircle;
double JoystickSetSteeringLastRearCircle;
const double JoystickSteeringTolerance = (5.0 / 360.0);


//  Joystick axes handler function
//
void VehicleBridge::OnJoystickAxisEvent(JoystickAxis axis, double x, double y)
{
	switch (axis)
	{
	case jsaLeftStick:	
		{
			if (JoystickMode == TwoWheelSteer)
			{
				auto steering =.25*x;
				if (fabs(steering - JoystickSetSteeringLastRearCircle) >= JoystickSteeringTolerance ||
					fabs(steering) < JoystickSteeringTolerance)
				{
					TheEngineRoom.SetRearSteering(steering);
					JoystickSetSteeringLastRearCircle = steering;
				}
			}
		}
		break;
		
	case jsaRightStick:	
		{
			if (JoystickMode == TwoWheelSteer)
			{
				auto steering = .25*x;
				if (fabs(steering - JoystickSetSteeringLastFrontCircle) >= JoystickSteeringTolerance ||
					fabs(steering) < JoystickSteeringTolerance)
				{
					TheEngineRoom.SetFrontSteering(steering);
					JoystickSetSteeringLastFrontCircle = steering;
				}
			}
			else
			{
				auto steering =.25*x;
				if (fabs(steering - JoystickSetSteeringLastFrontCircle) >= JoystickSteeringTolerance ||
					fabs(steering) < JoystickSteeringTolerance)
				{
					TheEngineRoom.SetFrontSteering(steering);
					TheEngineRoom.SetRearSteering(-steering);
					JoystickSetSteeringLastFrontCircle = steering;
					JoystickSetSteeringLastRearCircle = -steering;
				}
			}
		}
		break;
		
	case jsaTriggers:
		{
		}
		break;
		
	case jsaDpad:
		{
			SetTransmissionFromJoystickDPad(x, y);
		}
		break;
		
	default:
		break;
	}
}


//  Joystick buttons handler function
//
void VehicleBridge::OnJoystickButtonEvent(JoystickButton button, bool on)
{
	if (!on)
		return;
	
	switch (button)
	{
	case jsbA:
		{
			ChangeEnginePower(-0.01);
		}
		break;
		
	case jsbB:
		{
			ChangeEnginePower(0.01);
		}
		break;
		
	case jsbX:
		{
			ChangeEnginePower(-0.10);
		}
		break;
		
	case jsbY:
		{
			ChangeEnginePower(0.10);
		}
		break;
		
	case jsbLeftBumper:
		{
			TheEngineRoom.SetEnginePower(0.0);
		}
		break;
		
	case jsbRightBumper:
		{
			TheEngineRoom.FullStop();
		}
		break;
		
	case jsbMenu:
		{
			if (JoystickMode == TwoWheelSteer)
				JoystickMode = FourWheelSteer;
			else
				JoystickMode = TwoWheelSteer;
		
			Logging.AddLog("VehicleBridge", "JoystickButtonInput", format("Set steering mode %s", JoystickMode == FourWheelSteer ? "Four Wheel" : "Two Wheel"), LogLevelDebug);
		}
		
	default:
		break;
	}
}


//  Joystick connect / disconnect handler function
//
void VehicleBridge::OnJoystickConnected(string device, JoystickType type, bool connected)
{
	if (!connected)
		TheEngineRoom.FullStop();
}


void VehicleBridge::OnBrickButtonEvent(BrickButton button, bool state)
{
	Logging.AddLog("VehicleBridge", "OnBrickButtonEvent", format("Brick button %d is %s", button + 1, state ? "down" : "up"), LogLevelDebug);
	
	switch (button)
	{
	case bkbEnter:
		{
			if (!state)
				TheEngineRoom.FullStop();
		}
		break;
	case bkbLeft:
		{
			if (!state)
				TheEngineRoom.ResetTransmissionEncoder();
		}
		break;
	case bkbRight:
		{
			if (state)
				TheEngineRoom.SetTransmission(-1);
			else
				TheEngineRoom.FullStop();
		}
		break;
	case bkbBottom:
		{
			if (!state)
				TheEngineRoom.ResetSteeringEncoders();
		}
		break;
	default:
		Logging.AddLog("VehicleBridge", "OnBrickButtonEvent", "Unknown button.", LogLevelError);
		break;
	}
}
	

//  Steering Control
//
const double MaxSteeringCircle =.25;
const double MinSteeringCircle = -.25;

//  Change Front Steering
//
void VehicleBridge::ChangeFrontSteering(double changeCircle)
{
	double circle = TheEngineRoom.GetFrontSteering();
	circle += changeCircle;
	if (changeCircle > 0.0 && circle > MaxSteeringCircle)
		circle = MaxSteeringCircle;
	else if (changeCircle < 0.0 && circle < MinSteeringCircle)
		circle = MinSteeringCircle;
	
	TheEngineRoom.SetFrontSteering(circle);
}


//  Change rear steering
//
void VehicleBridge::ChangeRearSteering(double changeCircle)
{
	double circle = TheEngineRoom.GetRearSteering();
	circle += changeCircle;
	if (changeCircle > 0.0 && circle > MaxSteeringCircle)
		circle = MaxSteeringCircle;
	else if (changeCircle < 0.0 && circle < MinSteeringCircle)
		circle = MinSteeringCircle;
	
	TheEngineRoom.SetRearSteering(circle);
}


//  Change engine power
//
void VehicleBridge::ChangeEnginePower(double change)
{
	if (DrivingDirection == -1)
		change *= -1;
	
	double power = TheEngineRoom.GetEnginePower();
	power += change;
	if (DrivingDirection == 1 && power > 1.0)
		power = 1.0;
	else if (DrivingDirection == -1 && power < -1.0)
		power = -1.0;
	
	TheEngineRoom.SetEnginePower(power);
}


//  Transmission gear and driving direction set from DPad
//
unsigned int LastDpadPress = 0;
//
int DPadPress(double x, double y)
{
	if (y < -.9 && fabs(x) < 0.1)
		return 0;
	if (y < -.9 && x >.9)
		return 1;
	if (fabs(y) <.1 && x >.9)
		return 2;
	if (y > .9 && x > .9)
		return 3;
	if (y > .9 && fabs(x) < .1)
		return 4;
	if (y > .9 && x < -.9)
		return 5;
	if (fabs(y) < .1 && x < -.9)
		return 6;
	if (y < -.9 && x < -.9)
		return 7;
	
	return -1;
}

//  Set transmission from DPad
//
void VehicleBridge::SetTransmissionFromJoystickDPad(double x, double y)
{
	
	if (Millis() - LastDpadPress < 1000)
		return;
			
	int press = DPadPress(x, y);
	int gear = TheEngineRoom.GetTransmissionGear();
	if ( (press == 0 && gear == 2) || (press == 4 && gear == 1) )
		return;
	if ( (press == 6 && DrivingDirection == -1) || (press == 2 && DrivingDirection == 1) )
		return;
			
	LastDpadPress = Millis();
			
	switch (press)
	{
	case 0:
		{
			if (DrivingDirection == -1)
			{
				Logging.AddLog("VehicleBridge", "SetTransmissionFromJoystickDPad", "You can not set high gear in reverse direction", LogLevelWarn);
				return;
			}
			TheEngineRoom.SetTransmission(2);
		}
		break;
				
	case 1:
	case 3:
		{
			TheEngineRoom.SetTransmission(0);
		}
		break;
				
	case 4:
		{
			TheEngineRoom.SetTransmission(1);
		}
		break;
		
	case 7:
		{
			TheEngineRoom.ResetTransmissionEncoder();
		}
		break;
				
	case 5:
		{
			TheEngineRoom.ResetSteeringEncoders();
		}
		break;
		
	case 2:
		{
			if (fabs(TheEngineRoom.GetEnginePower()) > 0.0)
			{
				Logging.AddLog("VehicleBridge", "JoystickAxesInput", "Unable to change direction when motor power is on.", LogLevelWarn);
				return;
			}
			Logging.AddLog("VehicleBridge", "JoystickAxesInput", "Driving direction set to forward.", LogLevelInfo);
			DrivingDirection = 1;
		}
		break;
					
	case 6:
		{
			if (fabs(TheEngineRoom.GetEnginePower()) > 0.0)
			{
				Logging.AddLog("VehicleBridge", "JoystickAxesInput", "Unable to change direction when motor power is on.", LogLevelWarn);
				return;
			}
			if (TheEngineRoom.GetTransmissionGear() == 2)
			{
				TheEngineRoom.SetTransmission(1);
			}
			
			Logging.AddLog("VehicleBridge", "JoystickAxesInput", "Driving direction set to Reverse.", LogLevelInfo);
			DrivingDirection = -1;
		}
		break;		
	}
}




//  Get Engine Room Status
//
EngineRoomStatus VehicleBridge::GetEngineRoomStatus()
{
	EngineRoomStatus status(RobotName);
	status.EnginePower = TheEngineRoom.GetEnginePower();
	status.EngineMotor1Power  = TheEngineRoom.GetEngineMotor1Power();
	status.EngineMotor2Power = TheEngineRoom.GetEngineMotor2Power();
	status.EngineMotor1Rpm  = TheEngineRoom.GetEngineMotor1Rmp();
	status.EngineMotor2Rpm  = TheEngineRoom.GetEngineMotor2Rpm();
	status.Gear  = TheEngineRoom.GetTransmissionGear();
	status.Direction = DrivingDirection;
	status.FrontSteering =  TheEngineRoom.GetFrontSteering();
	status.RearSteering = TheEngineRoom.GetRearSteering();
	
	return status;
}


//  Get Button Status
//
ButtonStatus VehicleBridge::GetButtonStatus()
{
	ButtonStatus status(RobotName);
		
	//  TODO - status here
	status.LeftBtn = false;	
	status.EnterBtn = false;
	status.RightBtn = false;	
	status.DownBtn = false;
	
	return status;
}


//  Get Controller Status
ControllerStatus VehicleBridge::GetControllerStatus()
{
	ControllerStatus status(RobotName);

	status.CpuTemp = HardwareMonitor.GetMeanCpuTemp();
	status.CpuUsage = HardwareMonitor.GetMeanCpuUsage();
	status.MainBattVoltage = HardwareMonitor.GetMeanBatteryVoltage();
	
	//  TODO - joystick connected status
	
	return status;
}