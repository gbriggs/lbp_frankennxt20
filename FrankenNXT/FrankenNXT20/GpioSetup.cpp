#include <WiringPiExtensionApi.h>


#include "GpioSetup.h"
#include "Logger.h"
#include "StringExtensions.h"



using namespace std;


//  Setup wiringPi library, and initialize the GPIO pins used for this system
//
bool SetupGpio()
{
	//  setup wiring pi extension, using physical pin numbers
	int rc = SetupWiringPiExtension();
	
	if (rc != 0)
	{
		Logging.AddLog("GpioSetup.cpp", "SetupGpio", "Failed to setup wiring pi extension.", LogLevelFatal);
		return false;
	}
	
	//  setup SPI (for MCP3008 ADC chip)
	rc = WiringPiSPISetup(SPI_CHAN, 500000);

	//  initialize MCP3008 chip - pin numbers 100-107
	rc = Mcp3008Setup(MCP3008_BASE, SPI_CHAN);
	
	//  Adafruit DC Motor Hat at default address - pin numbers 200 - 215
	rc = Pca9685Setup(PCA9685_BASE, 0x60, 1500);
	if (rc == 0) 
	{
		Logging.AddLog("GpioSetup.cpp", "SetupGpio", "Failed to setup PCA9685.", LogLevelFatal);
		return false;
	}
	
	// turn all PCA pins off
	Pca9685PWMReset(rc); 
	
	//  setup the keypad buttons
	PinMode(PIN_KEYPADBUTTON, PINMODE_INPUT);
	
	//  led pin
	PinMode(PIN_LED, PINMODE_OUTPUT);
	DigitalWrite(PIN_LED, 1);
	
	
	Logging.AddLog("GpioSetup.cpp", "SetupGpio", "Finished setting up GPIO.", LogLevelInfo);
	
	return true;
}





