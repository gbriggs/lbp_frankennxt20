#pragma once
#include <functional>
#include "Thread.h"
#include "JoystickDataTypes.h"	


//  Joytick Events
typedef std::function<void(std::string,JoystickType,bool)> JoystickConnectedDelegateFn;
typedef std::function<void(JoystickButton, bool)> JoystickButtonEventDelegateFn;
typedef std::function<void(JoystickAxis, double, double)> JoystickAxesEventDelegateFn;


//  Joystick class implementation
class JoystickThread : public Thread
{
public:
	JoystickThread();
	
	virtual ~JoystickThread();
	
	void RegisterConnectedDelegate(JoystickConnectedDelegateFn delegate);
	void RegisterButtonEventDelegate(JoystickButtonEventDelegateFn delegate);
	void RegisterAxesEventDelegate(JoystickAxesEventDelegateFn delegate);
	
	bool IsConnected() { return JoystickFileDescriptor != -1; }
	
	virtual void Start();
	void Start(std::string device);
	
	virtual void Cancel();
	
	virtual void RunFunction();
	
protected:
	
	void Init();
	
	bool ConnectToJoystick();
	int JoystickFileDescriptor;
	
	void Connected(bool);
	void ButtonEvent(JoystickButton button, bool state);
	void AxisEvent(JoystickAxis axis, double x, double y);
	
	JoystickConnectedDelegateFn ConnectedDelegate;
	JoystickButtonEventDelegateFn ButtonEventDelegate;
	JoystickAxesEventDelegateFn AxisEventDelegate;
};