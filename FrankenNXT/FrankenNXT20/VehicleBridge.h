#pragma once

#include <string>
#include "JoystickDataTypes.h"
#include "BrickButtonDataTypes.h"
#include "HardwareMonitorThread.h"

class EngineRoomStatus;
class ButtonStatus;
class ControllerStatus;
class VehicleEngineRoom;

enum JoystickSteeringMode
{
	TwoWheelSteer,
	FourWheelSteer,
};


//  The Bridge
//  Control center of the robot, takes remote commands, monitors status, makes decisions

class VehicleBridge
{
public:
	
	VehicleBridge(VehicleEngineRoom& engineRoom);
	virtual ~VehicleBridge();
	
	//  Startup initialization function
	void Start();
	
	//  Shut down clean up function
	void ShutDown();
	
	//  Get status for different stations
	EngineRoomStatus GetEngineRoomStatus();
	ButtonStatus GetButtonStatus();
	ControllerStatus GetControllerStatus();
	
	//  Joystick
	void OnJoystickConnected(std::string device, JoystickType type, bool connected);
	void OnJoystickAxisEvent(JoystickAxis axis, double x, double y);
	void OnJoystickButtonEvent(JoystickButton button, bool on);
	
	//  Brick Buttons
	void OnBrickButtonEvent(BrickButton button, bool state);
	
	//  Request a command from keyboard input
	std::string OnKeyboardInputEvent(int key);
	
protected:
		
	//  A reference to the engine room object
	VehicleEngineRoom& TheEngineRoom;
	
	//  Driving forwards or backwards
	int DrivingDirection;
	
	//  Set front steering functions
	void ChangeFrontSteering(double changeCircle);
	void ChangeRearSteering(double changeCircle);
	
	//  Set engine power funcitons
	void ChangeEnginePower(double change);
	
	//  Set transmission functions
	void SetTransmissionFromJoystickDPad(double x, double y);
	
	//  Hardware monitor 
	HardwareMonitorThread HardwareMonitor;
	
	//  Joystick
	JoystickSteeringMode JoystickMode;
	
	//  Identity
	std::string RobotName;
};
